from __future__ import print_function
import sys, time

sys.path.insert(0, '../src')
from Robot import Robot
from AvoidObstaclePID import AvoidObstaclePID
from Navigation import Navigation

robot = Robot()
nav = Navigation(robot)
nav.start()
avoid_obstacle_pid = AvoidObstaclePID()

start_time = time.time()
simulation_time = 60 * 2

while time.time() - start_time < simulation_time:
  try:
    v_left, v_right = avoid_obstacle_pid.avoid(robot)
    robot.setLeftVelocity(v_left)
    robot.setRightVelocity(v_right)
    time.sleep(0.1)
  except:
    break

nav.stopThread()
print("Simulation Finished.")