import numpy as np
import math
from matplotlib import pyplot as plt

angle = 90
solution1 = np.load("followWallError_test1.npy", allow_pickle = True)
solution1 = [y for (x, y) in solution1]
solution2 = np.load("followWallError_test2.npy", allow_pickle = True)
solution2 = [y for (x, y) in solution2]
solution3 = np.load("followWallError_test3.npy", allow_pickle = True)
solution3 = [y for (x, y) in solution3]

solutionLength = min(len(solution1), len(solution2), len(solution3))

solution1 = solution1[0:solutionLength]
solution2 = solution2[0:solutionLength]
solution3 = solution3[0:solutionLength]

X1 = list(range(solutionLength))
GD = [1.3 for i in range(solutionLength)]

fig, ax = plt.subplots(figsize=(8, 8))
ax.plot(X1, solution1, c = 'red', marker = None, label="Test 1")
ax.plot(X1, solution2, c = 'blue', marker = None, label="Test 2")
ax.plot(X1, solution3, c = 'green', marker = None, label="Test 3")
ax.plot(X1, GD, c = 'black', marker = None, label="Expected Value")

ax.grid(True)
ax.legend()
ax.legend(loc='best')
plt.xlabel("Time")
plt.ylabel("Distance to the wall")
plt.title("Wall Following")
fig.savefig("FollowWallPIDError.png")
plt.show()