from __future__ import print_function
import sys, time

sys.path.insert(0, '../src')
from Robot import Robot
from FollowWallNeuralNetwork import FollowWallNeuralNetwork
from Navigation import Navigation

robot = Robot()
nav = Navigation(robot)
ctrl = FollowWallNeuralNetwork()
nav.start()

start_time = time.time()
simulation_time = 60 * 2

while time.time() - start_time < simulation_time:
  try:
    read_ultrassonic = robot.readUltrassonicSensors()
    V, W = ctrl.computeVelocity(read_ultrassonic[0:8])
    robot.setVelocity(V, W)
    time.sleep(0.1)
  except:
    break

nav.stopThread()
print("Simulation Finished.")