from __future__ import print_function

import random
import sys, time, cv2
from matplotlib import pyplot as plt
import numpy as np
import math
from Navigation import Navigation

sys.path.insert(0, '../src')
from Robot import Robot
from utils import *


robot = Robot()

nav = Navigation(robot)
nav.start()

global W

W = 0

training_data = []

#'pip3 install keyboard' command is necessary
#necessary to run as sudo and give accessibility permissions on MacOS
import keyboard  # using module keyboard

p = 0.0001
lastUpdate = time.time() * 1000
WHEEL_RADIUS = 0.195/2.0
while True:
    try:  # used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('q'):
            print('Quit')
            break
        if keyboard.is_pressed('a'): 
            W += p
        elif keyboard.is_pressed('d'):
            W -= p
        elif keyboard.is_pressed('w'):
            W = 0
        
        if W > 1.0:
            W = 1.0
        if W < -1.0:
            W = -1.0

        robot.setVelocity(WHEEL_RADIUS, W)
        if time.time() * 1000 - lastUpdate > 500:
            lastUpdate = time.time() * 1000
            ultrassonic = robot.read_ultrassonic_sensors()
            training_data.append((ultrassonic, W))
    except: 
        break  # if user pressed a key other than the given key the loop will break

robot.stop()
nav.stopThread()
print("training_data length", len(training_data))

old_training_data = np.load('training_data.npy')
np.save('training_data', training_data + old_training_data)