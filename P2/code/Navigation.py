import math, time, sys
import numpy as np
from threading import Thread

#Class responsible for controlling the velocity 
#of each wheel and update the position and angle of the robot (odometry information)
class Navigation(Thread):
    def __init__ (self, robot):
        Thread.__init__(self)
        x, y, z = robot.getCurrentPosition()
        self.xpos = x
        self.ypos = y
        alpha, beta, theta = robot.getCurrentOrientation()
        self.robot = robot
        self.theta = (theta + 2 * math.pi) % (2 * math.pi)
        self.theta_r = 0
        self.theta_l = 0
        self.theta_r_previous = 0
        self.theta_l_previous = 0
        self.d_r = 0
        self.d_l = 0
        self.l = 0.415-0.08
        self.radious = 0.195 / 2      
        self.interval = 0.05  #seconds
        self.odometry = [(x, y)]
        self.ground_truth = [(x, y)]
        self.stop = False
        self.floop = False
        self.useGroundTruth = False
        self.params = sys.argv
        if len(self.params) > 1 and self.params[1] == "useGroundTruth":
            print("Using GroundTruth")
            self.useGroundTruth = True
            

    def stopThread(self):
        self.stop = True
        self.robot.stop()
        if len(self.params) > 2:
            np.save(self.params[2] + "_odometry.npy", self.odometry)
            np.save(self.params[2] + "_gt.npy", self.ground_truth)

    def getOdometry(self):
        return self.odometry

    def getGroundTruth(self):
        return self.ground_truth

    def updateAngles(self):
        theta_l, theta_r = self.robot.getWheelsAngles()
        self.theta_r = theta_r
        self.theta_l = theta_l
    
    def updateVelocity(self, vl, vr):
        if self.floop:
            self.robot.setLeftVelocity(vl)
            self.robot.setRightVelocity(vr)
        else:
            self.robot.setRightVelocity(vr)
            self.robot.setLeftVelocity(vl)
        self.floop = not self.floop

    def update(self):
        self.updateAngles()

        #Convert Angles
        self.theta_r = (self.theta_r + 2 * math.pi) % (2 * math.pi)
        self.theta_l = (self.theta_l + 2 * math.pi) % (2 * math.pi)
        
        #Angle variation
        diff_theta_r = self.theta_r - self.theta_r_previous
        diff_theta_l = self.theta_l - self.theta_l_previous

        if abs(diff_theta_r) > 1:
            diff_theta_r = 0
        if abs(diff_theta_l) > 1:
            diff_theta_l = 0
            
        self.d_r = diff_theta_r * self.radious
        self.d_l = diff_theta_l * self.radious
        d_c = (self.d_l + self.d_r) / 2.0
        
        self.xpos = self.xpos + d_c * math.cos(self.theta)
        self.ypos = self.ypos + d_c * math.sin(self.theta)
        self.theta = self.theta + (self.d_r - self.d_l) / self.l
        
        # TODO: Check the following code 
        # if (abs(self.theta) > (2 * math.pi)):
        #     self.theta = 0
        self.theta = (self.theta + 2 * math.pi) % (2 * math.pi)
        
        self.theta_r_previous = self.theta_r
        self.theta_l_previous = self.theta_l
        
    def run(self):
        i = 0
        while not self.stop and self.robot.isConnected():
            self.update()
            i += 1
            if i % 50 == 0:
                self.odometry.append((self.xpos, self.ypos))
                x, y, z = self.robot.getCurrentPosition()
                self.ground_truth.append((x, y))
            if i % 100 == 0:
                print("Odometry:     ({0:.5f}, {1:.5f})".format(self.xpos, self.ypos))
                print("Ground Truth: ({0:.5f}, {1:.5f})".format(x, y))                
            time.sleep(self.interval)
        self.robot.stop()

    def getOrientation(self):
        if self.useGroundTruth:
            _, _, gamma = self.robot.getCurrentOrientation()
            return gamma
        else:
            return self.theta

    def getPosition(self):
        if self.useGroundTruth:
            x, y, z = self.robot.getCurrentPosition()
            return (x, y)
        else:
            return (self.xpos, self.ypos)