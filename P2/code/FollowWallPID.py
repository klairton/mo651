import math
import numpy as np

class FollowWallPID:

  def __init__(self):
    self.kp = 0.3
    self.ki = self.kp / 300.0
    self.kd = self.kp * 50.0
    self.i_error = 0
    self.diff_error = 0
    self.old_error = 0
    self.errorHistory = []

  def readSensor(self, robot):
    distances = [0 for _ in range(360)]
    reads = [0 for _ in range(360)]
    laser = robot.readLaserSensor()

    # Compute the distance
    for i in range(int(len(laser) / 3)):
        x = laser[3 * i]
        y = laser[3 * i + 1]
        d = (x ** 2 + y ** 2) ** (1.0 / 2.0)
        alpha = (math.atan2(y, x) + (2 * math.pi)) % (2 * math.pi) * (180 / math.pi)
        distances[int(alpha)] += d
        reads[int(alpha)] += 1

    # Average distance per angle
    for i in range(360):
      if reads[i]:
        distances[i] = distances[i] / reads[i]
      else:
        distances[i] = 3.0

    return distances

  def reset(self):
    self.i_error = 0
    self.diff_error = 0
    self.old_error = 0
    np.save("followWallError.npy", self.errorHistory)
    self.errorHistory = []

  def follow(self, robot, side):
  
    distances = self.readSensor(robot)
    lateral_distance = min(distances[240:360])if side == "R" else min(distances[0:120])
    frontal_distance = min(distances[0:30] + distances[330:360])

    if frontal_distance < 0.5 or lateral_distance > 2.0:
      return (0.0, 0.0)

    error = lateral_distance - 1.3
    
    self.errorHistory.append((1.3, lateral_distance))

    self.i_error += error
    self.diff_error = error - self.old_error
    self.old_error = error
    u = self.kp * error + self.ki * self.i_error + self.kd * self.diff_error

    if side == "R":
      return (2.0 + u, 2.0 - u)
    if side == "L":
      return (2.0 - u, 2.0 + u)
    return (0.0, 0.0)