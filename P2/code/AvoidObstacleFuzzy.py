# coding: utf-8
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import matplotlib.pyplot as plt
from RotatePID import RotatePID


class AvoidObstacleFuzzy():
    def __init__(self):
        self.max_speed = 0.5
        self.max_ang_velocity = 1.5
        self.min_wall_dist = 0
        self.max_wall_dist = 0.5
        self.steps = 0.01
        self.rotate_pid = RotatePID() 
        
    def init_fuzzy(self):
        '''
            Call this function to initialize the fuzzy control
        '''
        # Antecedents
        front_30  = ctrl.Antecedent(np.arange(0, self.max_wall_dist+0.1, self.steps), 'front30 wall distance')
        front_10  = ctrl.Antecedent(np.arange(0, self.max_wall_dist+0.1, self.steps), 'front10 wall distance')
        front_10n  = ctrl.Antecedent(np.arange(0, self.max_wall_dist+0.1, self.steps), 'front-10 wall distance')
        front_30n = ctrl.Antecedent(np.arange(0, self.max_wall_dist+0.1, self.steps), 'front-30 wall distance')

        antecedents = [front_30, front_10, front_10n, front_30n]

        for antecedent in antecedents:
            antecedent['very_close']       = fuzz.trimf(antecedent.universe, [0.0, 0.19, 0.25])
            antecedent['close']       = fuzz.trimf(antecedent.universe, [0.2, 0.3, 0.4])
            antecedent['almost_ok']      = fuzz.trimf(antecedent.universe, [0.40, 0.45, 0.45])
            antecedent['ok']       = fuzz.trimf(antecedent.universe, [0.45, 0.45, 0.51])
            # antecedent.view()
            

        # Consequents
        angular_velocity  = ctrl.Consequent(np.arange(-self.max_ang_velocity-0.1, self.max_ang_velocity+0.1, self.steps), 'angular velocity')
        linear_velocity = ctrl.Consequent(np.arange(-0.2, self.max_speed+0.1, self.steps), 'linear velocity')

        # Membership functions for angular_velocity
        angular_velocity['right_2'] = fuzz.trapmf(angular_velocity.universe, [-1.5, -1.5, -1.0, -0.5])
        angular_velocity['right_1'] = fuzz.trimf(angular_velocity.universe, [-0.55, -0.46, -0.3])
        angular_velocity['right_0'] = fuzz.trimf(angular_velocity.universe, [-0.45, -0.3, 0])
        angular_velocity['foward'] = fuzz.trapmf(angular_velocity.universe, [-0.004, 0, 0.005, 0.01])
        angular_velocity['left_0'] = fuzz.trimf(angular_velocity.universe, [0, 0.3, 0.45])
        angular_velocity['left_1'] = fuzz.trimf(angular_velocity.universe, [0.3, 0.45, 0.5])
        angular_velocity['left_2'] = fuzz.trapmf(angular_velocity.universe, [0.5, 1.0, 1.5, 1.5])
        self.angular_velocity = angular_velocity

        # Membership functions for linear_velocity
        linear_velocity['goback'] = fuzz.trimf(linear_velocity.universe, [-0.2, -0.1, 0])
        linear_velocity['stop'] = fuzz.trimf(linear_velocity.universe, [0.0, 0.0, 0.001])
        linear_velocity['very_slow'] = fuzz.trimf(linear_velocity.universe, [0.005, 0.1, 0.2])
        linear_velocity['slow'] = fuzz.trimf(linear_velocity.universe, [0.1, 0.2, 0.3])
        linear_velocity['normal'] = fuzz.trimf(linear_velocity.universe, [0.2, 0.3, 0.35])
        linear_velocity['fast'] = fuzz.trimf(linear_velocity.universe, [0.2, 0.3, 0.5])
        self.linear_velocity = linear_velocity


        # Fuzzy rules
        rules = {}

        rules['linear_rule1.a'] = ctrl.Rule(front_30['very_close'], linear_velocity['stop'])
        rules['linear_rule2.a'] = ctrl.Rule(front_30['close'], linear_velocity['slow'])
        rules['linear_rule3.a'] = ctrl.Rule(front_30['almost_ok'], linear_velocity['normal'])
        rules['linear_rule4.a'] = ctrl.Rule(front_30['ok'], linear_velocity['normal'])
        rules['gofast1'] = ctrl.Rule(front_30['ok'] & front_10['ok'], linear_velocity['fast'])
        rules['gofast2'] = ctrl.Rule(front_30n['ok'] & front_10n['ok'], linear_velocity['fast'])

        rules['linear_rule1.b'] = ctrl.Rule(front_10['very_close'], linear_velocity['stop'])
        rules['linear_rule2.b'] = ctrl.Rule(front_10['close'], linear_velocity['slow'])
        rules['linear_rule3.b'] = ctrl.Rule(front_10['almost_ok'], linear_velocity['normal'])
        rules['linear_rule4.b'] = ctrl.Rule(front_10['ok'], linear_velocity['normal'])

        rules['linear_rule1.c'] = ctrl.Rule(front_10n['very_close'], linear_velocity['stop'])
        rules['linear_rule2.c'] = ctrl.Rule(front_10n['close'], linear_velocity['slow'])
        rules['linear_rule3.c'] = ctrl.Rule(front_10n['almost_ok'], linear_velocity['normal'])
        rules['linear_rule4.c'] = ctrl.Rule(front_10n['ok'], linear_velocity['normal'])

        rules['linear_rule1.d'] = ctrl.Rule(front_30n['very_close'], linear_velocity['stop'])
        rules['linear_rule2.d'] = ctrl.Rule(front_30n['close'], linear_velocity['slow'])
        rules['linear_rule3.d'] = ctrl.Rule(front_30n['almost_ok'], linear_velocity['normal'])
        rules['linear_rule4.d'] = ctrl.Rule(front_30n['ok'], linear_velocity['normal'])

        rules['angular_rule1.a'] = ctrl.Rule(front_30['very_close'], angular_velocity['right_2'])
        rules['angular_rule2.a'] = ctrl.Rule(front_30['close'], angular_velocity['right_1'])
        rules['angular_rule3.a'] = ctrl.Rule(front_30['almost_ok'], angular_velocity['right_0'])
        rules['angular_rule4.a'] = ctrl.Rule(front_30['ok'], angular_velocity['foward'])


        rules['angular_rule1.b'] = ctrl.Rule(front_10['very_close'], angular_velocity['right_1'])
        rules['angular_rule2.b'] = ctrl.Rule(front_10['close'], angular_velocity['right_0'])
        rules['angular_rule3.b'] = ctrl.Rule(front_10['almost_ok'], angular_velocity['foward'])
        rules['angular_rule4.b'] = ctrl.Rule(front_10['ok'], angular_velocity['foward'])

        rules['angular_rule1.c'] = ctrl.Rule(front_10n['very_close'], angular_velocity['left_1'])
        rules['angular_rule2.c'] = ctrl.Rule(front_10n['close'], angular_velocity['left_0'])
        rules['angular_rule3.c'] = ctrl.Rule(front_10n['almost_ok'], angular_velocity['foward'])
        rules['angular_rule4.c'] = ctrl.Rule(front_10n['ok'], angular_velocity['foward'])

        rules['angular_rule1.d'] = ctrl.Rule(front_30n['very_close'], angular_velocity['left_2'])
        rules['angular_rule2.d'] = ctrl.Rule(front_30n['close'], angular_velocity['left_1'])
        rules['angular_rule3.d'] = ctrl.Rule(front_30n['almost_ok'], angular_velocity['left_0'])
        rules['angular_rule4.d'] = ctrl.Rule(front_30n['ok'], angular_velocity['foward'])

        rules['goback'] = ctrl.Rule(front_30n['very_close'] & front_10n['very_close'], angular_velocity['left_2'])
        rules['goback'] = ctrl.Rule(front_30n['very_close'] & front_10n['very_close'], linear_velocity['goback'])
        rules['goback'] = ctrl.Rule(front_30n['close'] & front_10n['close'], angular_velocity['left_1'])

        rules['goback'] = ctrl.Rule(front_10['close'] & front_30['close'], angular_velocity['right_1'])
        rules['goback'] = ctrl.Rule(front_10['very_close'] & front_30['very_close'], angular_velocity['right_2'])
        rules['goback'] = ctrl.Rule(front_10['very_close'] & front_30['very_close'], linear_velocity['goback'])

        # control simulation system
        sim_ctrl = ctrl.ControlSystem([rules[key] for key in rules])
        self.sim = ctrl.ControlSystemSimulation(sim_ctrl)

        # print('Fuzzy avoid obstacle initialized')


    def get_vel(self, distances, nav):
        '''
        	Returns the speed in both wheels
        	arg: distance in all 2 sensors (sensor 1 and sensor 4 respectively)
        	return: velocity tuple (left and front respectively)
        ''' 

        if max(distances[1:3]) < 0.2:
            self.rotate_pid.rotate(nav.robot, 30)

        for i in range(4):
            if distances[i] < self.min_wall_dist:
                distances[i] = self.min_wall_dist
            elif distances[i] > self.max_wall_dist:
                distances[i] = self.max_wall_dist

        self.sim.input['front30 wall distance'] = round(distances[0], 2)
        self.sim.input['front10 wall distance'] = round(distances[1], 2)
        self.sim.input['front-10 wall distance'] = round(distances[2], 2)
        self.sim.input['front-30 wall distance'] = round(distances[3], 2)

        self.sim.compute()
        return self.sim.output['linear velocity'], self.sim.output['angular velocity']
