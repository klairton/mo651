from __future__ import print_function
import sys, time

sys.path.insert(0, '../src')
from Robot import Robot
from FollowWallPID import FollowWallPID
from Navigation import Navigation

robot = Robot()
nav = Navigation(robot)
follow_wall_pid = FollowWallPID()
nav.start()

start_time = time.time()
simulation_time = 60 * 2

while time.time() - start_time < simulation_time:
  try:
	  v_left, v_right = follow_wall_pid.follow(robot, "L")
	  robot.setLeftVelocity(v_left)
	  robot.setRightVelocity(v_right)
	  time.sleep(0.1)
  except:
    break

nav.stopThread()
print("Simulation Finished.")

