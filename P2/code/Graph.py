# coding: utf-8
from matplotlib import pyplot as plt
import numpy as np
import sys

fileOdometry = sys.argv[1]
fileGT = sys.argv[2]
title = sys.argv[3].replace("_", " ")

odometry = np.load(fileOdometry)
ground_truth = np.load(fileGT)

X1 = []
Y1 = []
for x,y in odometry:
    X1.append(x)
    Y1.append(y)

X2 = []
Y2 = []
for x,y in ground_truth:
    X2.append(x)
    Y2.append(y)

fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(X1, Y1, c = 'tab:blue', marker = '.', label = "Odometria")
ax.scatter(X2, Y2, c = 'tab:green', marker = '.', label = "Ground Truth")
ax.grid(True)
ax.legend()
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.ylim(-10, 10)
plt.xlim(-10, 10) 
plt.title(title)
plt.show()
