import math, time, sys, random
sys.path.insert(0, '../src')
from Utils import *
from RotatePID import RotatePID

class FollowDoorPID:

    def __init__(self):
        self.kp = 0.001
        self.ki = self.kp / 50.0
        self.kd = self.kp * 10.0
        self.epsilon = 10 #centroid must be between 127 +- 10
        self.center = 127
        self.interval = 0.5
    
    def compute_centroid(self, points):
        c_x = 0.0
        c_y = 0.0
        for x, y in points:
            c_x += x
            c_y += y
        return (c_x / len(points), c_y / len(points))
        
    def door_detect(self, robot):
        resolution, raw_img = robot.read_vision_sensor()
        image = vrep2array(raw_img, resolution)
        lower = np.array((0, 0, 180), dtype = "uint8")
        upper = np.array((35, 35, 255), dtype = "uint8")
        mask = cv2.inRange(image, lower, upper)
        points = []
        for i in range(len(mask) - 1, -1, -1):
            for j in range(0, len(mask[i])):
                if mask[i][j] != 0:
                    points.append((j, i))
        return (len(points) > 400, points)

    def obstacle_detect_sonar(self, robot, sonars = [0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0], distance = 0.5):
        ultrassonic = robot.read_ultrassonic_sensors()
        detect = []
        for i in range(16):
            if sonars[i]:
                detect.append(ultrassonic[i] < distance)
            else:
                detect.append(False)
        return any(detect)

    def followDoor(self, robot, navigation):
        rotatePID = RotatePID()
        
        i_error = 0
        diff_error = 0
        old_error = 0
        door_found = False
        while not door_found:
            door, points = self.door_detect(robot)
            obstacle = self.obstacle_detect_sonar(robot, distance = 0.5)
            # print(door, obstacle)
            if door and not obstacle:
                c_x, c_y = self.compute_centroid(points)
                error = (self.center - c_x)
                i_error += error
                diff_error = error - old_error
                old_error = error
                if abs(error) > self.epsilon:
                    u = self.kp * error + self.ki * i_error + self.kd * diff_error
                    navigation.updateVelocity(-u, u)
                elif not obstacle:
                    navigation.updateVelocity(1, 1)
                else:
                    robot.stop()
            elif door:
                # print(door_found)
                robot.stop()
                door_found = True
            elif obstacle:
                rotatePID.rotate(robot, 45)
            else:
                navigation.updateVelocity(1, 1)   
                
            time.sleep(self.interval)