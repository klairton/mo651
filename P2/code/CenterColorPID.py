import math, time, sys, random
sys.path.insert(0, '../src')
from Utils import *

class CenterColorPID:
    def __init__(self):
        self.kp = 0.001
        self.ki = self.kp / 100.0
        self.kd = self.kp * 10.0
        self.epsilon = 10 #centroid must be between 127 +- 10
        self.center = 127
        self.interval = 0.5

    def computeCentroid(self, points):
        c_x = 0.0
        c_y = 0.0
        for x, y in points:
            c_x += x
            c_y += y
        return (c_x / len(points), c_y / len(points))

    def isCentered(self, points):
        c_x, c_y = self.computeCentroid(points)
        error = (self.center - c_x)
        return abs(error) < self.epsilon
    
    def doorDetect(self, robot):
        resolution, raw_img = robot.readVisionSensor()
        image = vrep2array(raw_img, resolution)
        lower = np.array((0, 0, 180), dtype = "uint8")
        upper = np.array((35, 35, 255), dtype = "uint8")
        mask = cv2.inRange(image, lower, upper)
        points = []
        for i in range(len(mask) - 1, -1, -1):
            for j in range(0, len(mask[i])):
                if mask[i][j] != 0:
                    points.append((j, i))
        return (len(points) > 400, points)

    def centerColor(self, nav):
        i_error = 0
        diff_error = 0
        old_error = 0
        while True:
            door, points = self.doorDetect(nav.robot)
            if len(points) == 0:
                break
            c_x, c_y = self.computeCentroid(points)
            error = (self.center - c_x)
            i_error += error
            diff_error = error - old_error
            old_error = error
            if abs(error) > self.epsilon:
                u = self.kp * error + self.ki * i_error + self.kd * diff_error
                nav.updateVelocity(-u, u)
            else:
                break
            time.sleep(self.interval)
        nav.robot.stop()

