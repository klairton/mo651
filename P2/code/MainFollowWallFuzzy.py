from __future__ import print_function
import sys, time

sys.path.insert(0, '../src')
from Robot import Robot
from FollowWallFuzzy import FollowWallFuzzy
from Navigation import Navigation

robot = Robot()
nav = Navigation(robot)
nav.start()
controller = FollowWallFuzzy()
controller.init_fuzzy()

start_time = time.time()
simulation_time = 60 * 2


while time.time() - start_time < simulation_time:
    try:
        read_ultrassonic = robot.readUltrassonicSensors(noDetectionDist=0.5)
        l, a = controller.get_vel(read_ultrassonic[0], read_ultrassonic[4], min(read_ultrassonic[2], read_ultrassonic[3]))
        if abs(a) < 0.01:
            a = 0
        robot.setVelocity(l, a)
        time.sleep(0.1)
    except: 
        break

nav.stopThread()
print("Simulation Finished.")
