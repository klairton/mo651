import math

class AvoidObstaclePID:

  def __init__(self):
    self.kp = 0.01
    self.ki = self.kp / 100.0
    self.kd = self.kp
    self.i_error = 0
    self.diff_error = 0
    self.old_error = 0
    
  def readSensor(self, robot):
    distances = [0 for _ in range(360)]
    reads = [0 for _ in range(360)]
    laser = robot.readLaserSensor()

    # Compute the distance
    for i in range(int(len(laser) / 3)):
        x = laser[3 * i]
        y = laser[3 * i + 1]
        d = (x ** 2 + y ** 2) ** (1.0 / 2.0)
        alpha = (math.atan2(y, x) + (2 * math.pi)) % (2 * math.pi) * (180 / math.pi)
        distances[int(alpha)] += d
        reads[int(alpha)] += 1

    # Average distance per angle
    for i in range(360):
      if reads[i]:
        distances[i] = distances[i] / reads[i]
      else:
        distances[i] = 5.0

    return distances

  def reset(self):
    self.i_error = 0
    self.diff_error = 0
    self.old_error = 0

  def avoid(self, robot):
    distances = self.readSensor(robot)
    frontal_distance = min(distances[0:30] + distances[330:360])

    error = sum(distances[0:90]) - sum(distances[270:360])

    if frontal_distance < 0.8:
      return (-1.0, 1.0)

    self.i_error += error
    self.diff_error = error - self.old_error
    self.old_error = error
    u = self.kp * error + self.ki * self.i_error + self.kd * self.diff_error
    return (2.0 - u, 2.0 + u)
