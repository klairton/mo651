from __future__ import print_function
import sys, time

sys.path.insert(0, '../src')
from Robot import Robot
from GoToGoalPID import GoToGoalPID
from Navigation import Navigation

robot = Robot()
nav = Navigation(robot)
go_to_goal = GoToGoalPID()
nav.start()
x , y = (0.0, 0.0)
c_x, c_y = nav.getPosition()
distance = ((x - c_x) ** 2 + (y - c_y) ** 2) ** (1.0 / 2.0)

while distance > 0.05:
  try:
	  v_left, v_right = go_to_goal.go(nav, (x, y))
	  robot.setLeftVelocity(v_left)
	  robot.setRightVelocity(v_right)
	  time.sleep(0.1)
	  c_x, c_y = nav.getPosition()
	  distance = ((x - c_x) ** 2 + (y - c_y) ** 2) ** (1.0 / 2.0)
  except: 
    break

nav.stopThread()
print("Simulation Finished.")
