import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPRegressor

class FollowWallNeuralNetwork:

    def __init__(self):
        WHEEL_RADIUS = 0.195/2.0
        self.V = WHEEL_RADIUS 

        data = np.load("training_data.npy", allow_pickle = True)

        X = data[:, 0]
        X = np.array([np.array(xi[0:8]) for xi in X])
        Y = data[:, 1]
        Y = np.array(Y)

        # Neural network parameters
        rn_params = {
                    'max_iter': [1000],
                    'activation' : ['identity', 'logistic', 'tanh', 'relu'],
                    'solver' : ['lbfgs', 'sgd', 'adam'],
                    'hidden_layer_sizes': [
                    (4,),(5,),(6,),(7,),(8,),(9,),(10,),(11,),(12,),(13,),(14,),(15,)
                    ]
                }

        
        # best parameters found
        hidden_layer_sizes, activation, solver = (8,), 'logistic', 'sgd'

        self.mlpl = MLPRegressor(random_state = 1, hidden_layer_sizes=(8,), solver=solver, activation=activation, max_iter = 1000)
        self.mlpl.fit(X,Y)

    def computeVelocity(self, distances):
        W = self.mlpl.predict([distances[0:8]])  
        # Scaling W and V by 2
        return (self.V * 2.0, W * 2.0)