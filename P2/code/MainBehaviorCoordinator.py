from __future__ import print_function
import pandas as pd
import numpy as np

import random
import sys, time, cv2
import numpy as np
import math
from Navigation import Navigation

sys.path.insert(0, '../src')
from Robot import Robot
from Utils import *

from FollowWallNeuralNetwork import FollowWallNeuralNetwork
from AvoidObstacleFuzzy import AvoidObstacleFuzzy
from GoToGoalPID import GoToGoalPID

ctrlFollowWall = FollowWallNeuralNetwork()
ctrlAvoidObstacle = AvoidObstacleFuzzy()
ctrlAvoidObstacle.init_fuzzy()
ctrlGoToGoal = GoToGoalPID()

robot = Robot()
nav = Navigation(robot)
nav.start()

goals = [[0.0, 0.0], [-5.0, 0.0], [-5.0, 1.5]]

actGoToGoal = 'goToGoal'
actFollowWall = 'followWall'
actAvoidObstacle = 'actAvoidObstacle'


AVOID_OBSTACLE_ACTIVATOR = 0.3
FOLLOW_WALL_ACTIVATOR = 1
interval = 0.1
epsilon = 0.05

goal = goals[0]
c_x, c_y = nav.getPosition()
old_distance = ((goal[0] - c_x) ** 2 + (goal[1] - c_y) ** 2) ** (1.0 / 2.0)
old_act = ''

while len(goals) > 0:
    
    act = actGoToGoal
    dist = robot.readUltrassonicSensors()[0:8]

    if min(dist[2:6]) < AVOID_OBSTACLE_ACTIVATOR:
        act = actAvoidObstacle
    elif min(dist[0:2] + dist[6:8]) < FOLLOW_WALL_ACTIVATOR:
        act = actFollowWall
    
    c_x, c_y = nav.getPosition()
    distance = ((goal[0] - c_x) ** 2 + (goal[1] - c_y) ** 2) ** (1.0 / 2.0)
    if distance < epsilon:
        goals = goals[1:len(goals)]
        if len(goals) == 0:
            break
        goal = goals[0]
        ctrlGoToGoal.reset()
        continue
        #Go to next goal point

    if old_act == actFollowWall and act == actFollowWall and old_distance < distance:
        act = actGoToGoal

    old_distance = distance
    old_act = act
    print(act)

    if act == actFollowWall:
        l, a = ctrlFollowWall.computeVelocity(dist)
        if abs(a) < 0.01:
            a = 0
        robot.setVelocity(l, a)
    elif act == actAvoidObstacle:
        l, a = ctrlAvoidObstacle.get_vel(dist[2:6], nav)
        if abs(a) < 0.01:
            a = 0
        robot.setVelocity(l, a)
    else:
        vl, vr = ctrlGoToGoal.go(nav, goal)
        print(vl,vr)
        nav.updateVelocity(vl, vr)

    time.sleep(interval)


nav.stopThread()