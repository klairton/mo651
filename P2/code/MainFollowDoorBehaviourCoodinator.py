from __future__ import print_function
import pandas as pd
import numpy as np

import random
import sys, time, cv2
from matplotlib import pyplot as plt
import numpy as np
import math
from Navigation import Navigation

sys.path.insert(0, '../src')
from Robot import Robot
from Utils import *

from CenterColorPID import CenterColorPID
from AvoidObstacleFuzzy import AvoidObstacleFuzzy

ctrlCenterColor = CenterColorPID()
ctrlAvoidObstacle = AvoidObstacleFuzzy()
ctrlAvoidObstacle.init_fuzzy()

robot = Robot()
nav = Navigation(robot)
nav.start()

door_found = False
AVOID_OBSTACLE_ACTIVATOR = 0.5

actMoveFoward = 'actMoveFoward'
actCenterColor = 'actCenterColor'
actAvoidObstacle = 'actAvoidObstacle'
act = actMoveFoward

while not door_found:
    door, points = ctrlCenterColor.door_detect(robot)
    dist = robot.readUltrassonicSensors()[0:8]

    isCentered = ctrlCenterColor.isCentered(points)
    
    #try to detect if two door are in the image
    minX = points[0][0]
    maxX = points[0][0]
    for p in points:
        minX = min(p[0], minX)
        maxX = max(p[0], maxX)
    diffPoints = maxX - minX

    if door and min(dist[2:6]) < AVOID_OBSTACLE_ACTIVATOR:
        #door found
        door_found = True
        continue
    elif min(dist[2:6]) < AVOID_OBSTACLE_ACTIVATOR:
        act = actAvoidObstacle
    elif not door:
        act = actMoveFoward
    elif not isCentered and diffPoints < 50:
        act = actCenterColor
    else:
        act = actMoveFoward

    print(act)

    if act == actCenterColor:
        ctrlCenterColor.centerColor(nav)
    elif act == actAvoidObstacle:
        l, a = ctrlAvoidObstacle.get_vel(dist, nav)
        robot.setVelocity(l, a)
        time.sleep(0.1)
    elif act == actMoveFoward:
        nav.updateVelocity(1.5, 1.5)
        time.sleep(0.1)


nav.stopThread()

