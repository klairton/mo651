# coding: utf-8
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

class FollowWallFuzzy():
    def __init__(self):
        self.max_speed = 0.5
        self.max_ang_velocity = 1.5
        self.min_wall_dist = 0
        self.max_wall_dist = 0.5
        self.steps = 0.01

    def init_fuzzy(self):
        '''
            Call this function to initialize the fuzzy control
        '''

        # Antecedents
        left_wall_dist  = ctrl.Antecedent(np.arange(0.15, self.max_wall_dist+0.1, self.steps), 'left wall distance')
        front_wall_dist = ctrl.Antecedent(np.arange(0.15, self.max_wall_dist+0.1, self.steps), 'front wall distance')
        diagonal_wall_dist = ctrl.Antecedent(np.arange(0.15, self.max_wall_dist+0.1, self.steps), 'diagonal wall distance')

        # Consequents
        angular_velocity  = ctrl.Consequent(np.arange(-self.max_ang_velocity-0.1, self.max_ang_velocity+0.1, self.steps), 'angular velocity')
        linear_velocity = ctrl.Consequent(np.arange(-0.2, self.max_speed+0.1, self.steps), 'linear velocity')

        # Membership functions for left_wall_distance
        left_wall_dist['very_close']       = fuzz.trapmf(left_wall_dist.universe, [0, 0.11, 0.19, 0.25])
        left_wall_dist['close']       = fuzz.trimf(left_wall_dist.universe, [0.22, 0.30, 0.35])
        left_wall_dist['almost_ok']      = fuzz.trimf(left_wall_dist.universe, [0.3, 0.35, 0.40])
        left_wall_dist['ok']       = fuzz.trimf(left_wall_dist.universe, [0.38, 0.40, 0.45])
        left_wall_dist['distant'] = fuzz.trapmf(left_wall_dist.universe, [0.43, 0.45, 0.51, 0.51])
        
        # # Membership functions for diagonal_wall_distance
        diagonal_wall_dist['very_close']       = fuzz.trapmf(diagonal_wall_dist.universe, [0, 0.11, 0.19, 0.25])
        diagonal_wall_dist['close']       = fuzz.trimf(diagonal_wall_dist.universe, [0.22, 0.30, 0.35])
        diagonal_wall_dist['almost_ok']      = fuzz.trimf(diagonal_wall_dist.universe, [0.3, 0.35, 0.40])
        diagonal_wall_dist['ok']       = fuzz.trimf(diagonal_wall_dist.universe, [0.38, 0.40, 0.45])
        diagonal_wall_dist['distant'] = fuzz.trapmf(diagonal_wall_dist.universe, [0.43, 0.45, 0.51, 0.51])


        # Membership functions for front_wall_distance
        front_wall_dist['very_close']       = fuzz.trapmf(front_wall_dist.universe, [0, 0.11, 0.19, 0.25])
        front_wall_dist['close']       = fuzz.trimf(front_wall_dist.universe, [0.22, 0.30, 0.35])
        front_wall_dist['almost_ok']      = fuzz.trimf(front_wall_dist.universe, [0.3, 0.35, 0.40])
        front_wall_dist['ok']       = fuzz.trimf(front_wall_dist.universe, [0.38, 0.40, 0.45])
        front_wall_dist['distant'] = fuzz.trapmf(front_wall_dist.universe, [0.43, 0.45, 0.50, 0.51])

        # Membership functions for angular_velocity
        angular_velocity['right_2'] = fuzz.trapmf(angular_velocity.universe, [-1.5, -1.3, -0.5, -0.4])
        angular_velocity['right_1'] = fuzz.trimf(angular_velocity.universe, [-0.55, -0.46, -0.3])
        angular_velocity['right_0'] = fuzz.trimf(angular_velocity.universe, [-0.45, -0.3, 0])
        angular_velocity['foward'] = fuzz.trapmf(angular_velocity.universe, [-0.004, 0, 0.005, 0.01])
        angular_velocity['left_0'] = fuzz.trimf(angular_velocity.universe, [0, 0.3, 0.45])
        angular_velocity['left_1'] = fuzz.trimf(angular_velocity.universe, [0.3, 0.45, 0.5])
        angular_velocity['left_2'] = fuzz.trapmf(angular_velocity.universe, [0.4, 0.5, 1.5, 1.5])
        self.angular_velocity = angular_velocity

        # Membership functions for linear_velocity
        linear_velocity['back'] = fuzz.trimf(linear_velocity.universe, [-0.01, 0.0, 0.0])
        linear_velocity['stop'] = fuzz.trimf(linear_velocity.universe, [0.0, 0.0, 0.001])
        linear_velocity['very_slow'] = fuzz.trimf(linear_velocity.universe, [0.005, 0.1, 0.2])
        linear_velocity['slow'] = fuzz.trimf(linear_velocity.universe, [0.1, 0.2, 0.3])
        # linear_velocity['normal'] = fuzz.trimf(linear_velocity.universe, [0.2, 0.3, 0.35])
        linear_velocity['fast'] = fuzz.trimf(linear_velocity.universe, [0.2, 0.3, 0.5])
        # linear_velocity['very_fast'] = fuzz.trimf(linear_velocity.universe, [0.3, 0.35, 0.5])
        self.linear_velocity = linear_velocity
        
        # Fuzzy rules
        rules = {}

        rules['rule1'] = ctrl.Rule(front_wall_dist['close'], linear_velocity['stop'])
        rules['rule2'] = ctrl.Rule(front_wall_dist['almost_ok'], linear_velocity['stop'])
        rules['rule3'] = ctrl.Rule(front_wall_dist['ok'], linear_velocity['slow'])
        rules['rule4'] = ctrl.Rule(front_wall_dist['distant'], linear_velocity['back'])
        
        rules['rule5'] = ctrl.Rule(left_wall_dist['close'], linear_velocity['fast'])
        rules['rule6'] = ctrl.Rule(left_wall_dist['almost_ok'], linear_velocity['slow'])
        rules['rule7'] = ctrl.Rule(left_wall_dist['ok'], linear_velocity['fast'])
        rules['rule8'] = ctrl.Rule(left_wall_dist['distant'], linear_velocity['slow'])
        
        rules['rule9'] = ctrl.Rule(left_wall_dist['distant'] & front_wall_dist['distant'], linear_velocity['stop'])

        rules['rule10'] = ctrl.Rule(front_wall_dist['close'], angular_velocity['right_2'])
        rules['rule11'] = ctrl.Rule(front_wall_dist['almost_ok'], angular_velocity['right_2'])
        rules['rule12'] = ctrl.Rule(front_wall_dist['ok'], angular_velocity['foward'])
        rules['rule13'] = ctrl.Rule(front_wall_dist['distant'], angular_velocity['foward'])
        
        rules['rule14'] = ctrl.Rule(left_wall_dist['close'], angular_velocity['right_0'])
        rules['rule15'] = ctrl.Rule(left_wall_dist['almost_ok'], angular_velocity['right_0'])
        rules['rule16'] = ctrl.Rule(left_wall_dist['ok'], angular_velocity['foward'])
        rules['rule17'] = ctrl.Rule(left_wall_dist['distant'], angular_velocity['left_0'])
        
        rules['rule18'] = ctrl.Rule(left_wall_dist['distant'] & front_wall_dist['distant'], angular_velocity['left_1'])
        # rules['rule19'] = ctrl.Rule(diagonal_wall_dist['ok'], angular_velocity['foward'])
        rules['rule19'] = ctrl.Rule(diagonal_wall_dist['close'], angular_velocity['right_0'])
        rules['rule20'] = ctrl.Rule(diagonal_wall_dist['very_close'], angular_velocity['right_2'])

        rules['rule21'] = ctrl.Rule(left_wall_dist['very_close'], angular_velocity['right_0'])
        rules['rule22'] = ctrl.Rule(left_wall_dist['very_close'] & front_wall_dist['very_close'], angular_velocity['right_1'])
        rules['rule23'] = ctrl.Rule(left_wall_dist['very_close'], linear_velocity['slow'])

        rules['rule24'] = ctrl.Rule(left_wall_dist['very_close'] & front_wall_dist['very_close'] & 
        diagonal_wall_dist['very_close'], angular_velocity['right_2'])
        rules['rule25'] = ctrl.Rule(left_wall_dist['very_close'] & front_wall_dist['very_close'] & 
        diagonal_wall_dist['very_close'], linear_velocity['back'])


        # Sistema de controle e simulação
        sim_ctrl = ctrl.ControlSystem([rules[key] for key in rules])
        self.sim = ctrl.ControlSystemSimulation(sim_ctrl)

        # print('Fuzzy wall follow initialized')

    def get_vel(self, left_wall_dist, front_wall_dist, diagonal_wall_dist):
        '''
        	Returns the speed in both wheels
        	arg: distance in all 2 sensors (sensor 1 and sensor 4 respectively)
        	return: velocity tuple (left and front respectively)
        '''

        if left_wall_dist < self.min_wall_dist:
            left_wall_dist = self.min_wall_dist
        elif left_wall_dist > self.max_wall_dist:
            left_wall_dist = self.max_wall_dist

        if front_wall_dist < self.min_wall_dist:
            front_wall_dist = self.min_wall_dist
        elif front_wall_dist > self.max_wall_dist:
            front_wall_dist = self.max_wall_dist

        if diagonal_wall_dist < self.min_wall_dist:
            diagonal_wall_dist = self.min_wall_dist
        elif diagonal_wall_dist > self.max_wall_dist:
            diagonal_wall_dist = self.max_wall_dist
            

        self.sim.input['left wall distance'] = round(left_wall_dist, 2)
        self.sim.input['front wall distance'] = round(front_wall_dist, 2)
        self.sim.input['diagonal wall distance'] = round(diagonal_wall_dist, 2)

        self.sim.compute()
        return self.sim.output['linear velocity'], self.sim.output['angular velocity']
