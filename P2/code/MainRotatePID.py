from __future__ import print_function
import random
import sys, time
import math

sys.path.insert(0, '../src')
from Robot import Robot
from RotatePID import RotatePID

ITERATIONS = 5
robot = Robot()
rotate_pid = RotatePID()

for i in range(ITERATIONS):
    print("Iteration {0} of {1}".format(i, ITERATIONS))

    # Showing initial angle
    _, _, gamma = robot.getCurrentOrientation()
    gamma = (gamma + 2 * math.pi) % (2 * math.pi)
    print("Robot initial angle: {0:.5f}".format(gamma * 180 / math.pi))
    angle = random.choice([-1.0, 1.0]) * random.randrange(0, 180)
    print("Rotating robot {0} degrees.".format(angle))

    # Rotating the robot
    rotate_pid.rotate(robot, angle * math.pi / 180)

    # Showing final angle
    _, _, gamma = robot.getCurrentOrientation()
    gamma = (gamma + 2 * math.pi) % (2 * math.pi)
    print("Robot final angle:   {0:.5f}".format(gamma * 180 / math.pi))
    print("---------------------")
    time.sleep(1.0)

print("Simulation Finished.")

