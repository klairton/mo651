Given a map of the environment, an origin point A, a target point B and an objective function $f$, the goal of the path planning problem is to find a set of points $P$ such that: (i) for each pair of consecutive points X and Y in $P$, the robot can move from $X$ to $Y$ without hitting an obstacle and (ii) the value of $f(P)$ is minimized.

\textbf{Input:}  A list of obstacles that describes the map of the environment. An origin point $A = (x_a, y_a)$. A target point $B = (x_b, y_b)$. An objective function $f$ that evaluates a set of points.

\textbf{Output:}  A set of points $P$ such that: (i) for each pair of consecutive points X and Y in $P$, the robot can move from $X$ to $Y$ without hitting an obstacle and (ii) the value of $f(P)$ is minimized.

A simple objective function is to use only the sum of the distances between consecutive points of the path, which is an approximation of the real distance performed by the robot, since the robot may not be able to always move in a straight line to move towards the goal. Therefore, we included the smoothness and safety criteria to the objective function.

The smoothness criteria is related to the rotation degree needed for the robot to go from a point X to a point Y in the path, for each pair of consecutive points. As a motivation for this criteria, if the robot does only small rotations, then the angle error in the odometry can be minimized.

The safety criteria is related to the robot's distance to obstacles while executing the path. Although a feasible path prevents collision in a higher level, it can demand the robot to be very close to an obstacle while going towards a goal. Due to errors in odometry calculation or in the movement controller, this can cause collisions when executing a trajectory.

Given real value positive constants $\alpha$, $\beta$, and $\gamma$, the objective function $w$ is used to evaluate a path $P = p_1, p_2, \ldots, p_\ell$ and defined as:

\small{
\begin{itemize}
  \item $w(P) = {(\alpha \times distance(P))} + {(\beta \times smoothness(P))} - {(\gamma \times safety(P))}$
  \vspace{.3cm}
  \item $distance(P) = \sum_{i = 1}^{\ell - 1} \sqrt{(p_{i}.x - p_{i+1}.x)^2 + (p_{i}.y - p_{i+1}.y)^2}$
  \vspace{.3cm}
  \item $smoothness(P) = \sum_{i = 1}^{\ell - 2} (rotate(p_{i}, p_{i + 1}, p_{i + 2}) / (\pi / 3.0))^{2}$
  \vspace{.3cm}
  \item $safety(P) = \min \cup_{i = 1}^{\ell - 1} obstacle(p_{i}, p_{i+1})$
  \vspace{.3cm}
\end{itemize}
}

Where $rotate(p_{i}, p_{i + 1}, p_{i + 2})$ is equal to the minimum rotation degree the robot has to perform to go from $p_i$ to $p_{i+2}$ (in this work, the starting angle of the robot in $p_0$ is considered as zero), and $obstacle(p_{i})$ is equal to the minimum distance from the segment that goes from $p_i$ to $p_{i+1}$ to an obstacle in the scene.

The length criteria is equal to the number of points in the path. Trying to reduce the length of the robot is useful to reduce the number of redundant points in the trajectory.

The following steps are done for solving this problem:

\begin{enumerate}
  \item Model the environment;
  \item Obtain a feasible initial solution using only the distance objective function. This solution is used by the metaheuristics;
  \item Define objective functions with the three criteria presented;
  \item Develop algorithms using the metaheuristics Ant Colony, Tabu Search, and Genetic Algorithms, and considering the objective functions of the previous step;
  \item Smooth the trajectory (heuristic implemented in the Genetic Algorithm);
  \item Execute experiments in the V-REP simulator to evaluate the solutions.
\end{enumerate}

