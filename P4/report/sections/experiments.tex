In this section, we initially present the scenarios in which our heuristics were applied and then the results obtained by heuristics \textit{Ant Colony}, \textit{Tabu Search}, and \textit{Genetic Algorithm} are presented.

\subsection{Scenarios}

In order to test our heuristics in different environments, we created four scenarios with different characteristics. All scenarios have the same dimensions (10 meters x 10 meters). Obstacles have been added in the sphere format to facilitate collision checking, but the heuristics can be easily adapted to any obstacle format. Figure~\ref{tab:scenarios} shows the scenarios that were created using the V-REP simulator.

\begin{figure*}[h!]
    \begin{center}
      \includegraphics[width=0.35\textwidth]{figuras/scenarios/SC01.png} \hspace{1cm}
      \includegraphics[width=0.35\textwidth]{figuras/scenarios/SC02.png}

      \vspace{1cm}

      \includegraphics[width=0.35\textwidth]{figuras/scenarios/SC03.png} \hspace{1cm}
      \includegraphics[width=0.35\textwidth]{figuras/scenarios/SC04.png}
      \caption{The Image shows scenarios \textbf{SC01}, \textbf{SC02}, \textbf{SC03}, and \textbf{SC04} positioned at the top left, top right, bottom left and bottom right, respectively.}
      \label{tab:scenarios}
    \end{center}
\end{figure*}

Next, we present some of the characteristics of each scenario presented in Figure~\ref{tab:scenarios}:
\begin{itemize}
	\item[SC01] This is a simple scenario with wide space and that allows us to obtain several paths.
	\item[SC02] This scenario is more restricted considering the diversity of paths, but there is still wide space between the obstacles.
	\item[SC03] This scenario is the most complex, there are different obstacles distributed by the scenarios, and two of them limit the passage of the robot through a very tight section.
	\item[SC04] This scenario is simple, but it allows heuristics to choose a longer and safer path or a shorter path with a section where there are nearby obstacles.
    \vspace{.3cm}
\end{itemize}

Table~\ref{tab:scenarios-points} shows the origin and destination points chosen in each scenario.

\begin{table}[h!]
\centering
\caption{Origin and destination points in each scenario.}
\begin{tabular}{@{}|c|c|c|@{}}
\toprule
Scenario & Start point & Destination point \\ \midrule
SC01  & (1.0, 9.0)   & (9.0, 1.0)        \\ \midrule
SC02  & (1.0, 9.0)   & (1.0, 1.0)        \\ \midrule
SC03  & (1.0, 9.0)   & (9.0, 1.0)        \\ \midrule
SC04  & (2.5, 2.5)   & (7.5, 2.5)        \\ \bottomrule
\end{tabular}
\label{tab:scenarios-points}
\end{table}


\subsection{Results}
In this subsection we present the results obtained by each of our heuristics. The solutions obtained were applied in each scenario using a controller that directs the robot to a target on the map (point). Sequentially each point of an obtained path was assigned to that controller until the destination point was reached in the V-REP simulator. For each scenario and solution provided by our heuristics a video has been recorded and all are accessible through the following link:\\
\textbf{\scriptsize https://drive.google.com/open?id=1hIT0Gl7LgLZgcFHTyhNngCb8EuwTFD3N}

In the following graphs we represent the obstacles of a scenario as spheres in yellow and the path obtained as a line in blue.

\subsubsection{Ant Colony}

The Ant Colony heuristic was performed for 100 iterations using 30 ants and a pheromone evaporation coefficient of $0.8$, the results obtained can be seen in Figure~\ref{tab:result-ACO}.

\begin{figure*}[!htb]
    \centering
    \includegraphics[width=0.37\textwidth]{figuras/ACO/SC01.png}
    \includegraphics[width=0.37\textwidth]{figuras/ACO/SC02.png}
    \includegraphics[width=0.37\textwidth]{figuras/ACO/SC03.png}
    \includegraphics[width=0.37\textwidth]{figuras/ACO/SC04.png}
    \caption{Ant Colony solutions for scenarios \textbf{SC01} (top left), \textbf{SC02} (top right), \textbf{SC03} (bottom left), and \textbf{SC04} (bottom right).}
    \label{tab:result-ACO}
\end{figure*}

Figure~\ref{tab:result-ACO} shows that the heuristic can find a suitable path, but the rotation angles are very pronounced making the path quite abrupt. In addition, the heuristics in scenario \textbf{SC04} did not choose to take the shortest path.

\subsubsection{Tabu Search}
To achieve the initial solution, the best one hundred and fifty iterations of the grid search heuristc algorithm were used. The Tabu Search was performed for 150 iterations and each move consists of changing the X and Y coordinate of a point, adding a point or removing a point from the solution. Each movement was used the technique of best improving, ie the exchange chosen is the best of all explored. The Tabu Search size chosen for the chosen experiments was seventy. This size was chosen because of several pre-tests performed, where it was the size that best came out of those tested.



The objective function used in Tabu Search was as follows: $w(P) = {(1.0 \times distance(P))} + {(1.0 \times smoothness(P))} -  {(250 \times safety(P))}$. The results obtained can be seen in Figure~\ref{tab:result-TS}. Figure~\ref{tab:result-TS} shows that the heuristic, in comparison with the Ant Colony heuristic, was able to perform a path using smoother angles. However, we can notice some abrupt deviations in the middle of the path, this is more evident in scenarios \textbf{SC03} and \textbf{SC04}.

\begin{figure*}[!htb]
    \centering
    \includegraphics[width=0.37\textwidth]{figuras/TS/SC01.png}
    \includegraphics[width=0.37\textwidth]{figuras/TS/SC02.png}
    \includegraphics[width=0.37\textwidth]{figuras/TS/SC03.png}
    \includegraphics[width=0.37\textwidth]{figuras/TS/SC04.png}
    \caption{Tabu Search solutions for scenarios \textbf{SC01} (top left), \textbf{SC02} (top right), \textbf{SC03} (bottom left), and \textbf{SC04} (bottom right).}
    \vspace{1cm}
    \label{tab:result-TS}
\end{figure*}



\subsubsection{Genetic Algorithm}

The Genetic Algorithm heuristic was the one that presented the best results and for this reason we performed some extra experiments using this approach considering two different objective functions. The heuristics was tuned using the following parameters:

\begin{itemize}
    \item INITIAL\_POPULATION = 100
    \item SELECTION = 50
    \item CROSSOVER = 50\%
    \item MUTATION = 50\%
    \item SMOOTH = 10\%
    \item PATH\_COMPRESSION = 50\%
    \item AVOID\_COLLISION = 25\%
    \item GENERATIONS = 100
\end{itemize}

For the fitness function, we considered two objective functions for the characteristics that were being assessed in the solutions (distance, smoothness, safety), namely:

\begin{itemize}
    \item $w_{1}(P) = {(1.0 \times distance(P))} + {(1.0 \times smoothness(P))} -  {(250 \times safety(P))}$
    \item $w_{2}(P) = {(1.0 \times distance(P))} + {(1.0 \times smoothness(P))} -  {(25 \times safety(P))}$
\end{itemize}

We can see that function $w_{1}$ considers the safety of the path much more important than function $w_{2}$. We compared the results obtained using these two functions and showed some interesting behaviors. To determine an appropriate value for the maximum number of generations of the algorithm, we perform a test that consists of applying the algorithm for 200 generations and in each of them the best score obtained is stored. In Table~\ref{tab:GA-generations}, we can see this information considering the two objective functions and the generations 1, 50, 100, 150, and 200. It is possible to see that from the generation 100 to the generation 200, the score value obtained does not change significantly. For this reason, we adopted 100 as the maximum number of generations of the algorithm. We performed a similar test to determine the size of the population, but we noticed that the size of the population reflects the amount of processing performed for collision detection and it directly impacted on the runtime of the algorithm. Thus, in the trade-off between runtime feasibility and solution quality, we set the population size as 50.

\begin{table*}[!htb]
\centering
\caption{Experiment of best score obtained by generation.}
\begin{tabular}{@{}|c|c|c|c|c|c|c|c|c|@{}}
\toprule
\multirow{2}{*}{Generation} & \multicolumn{2}{c|}{SC01}               & \multicolumn{2}{c|}{SC02}               & \multicolumn{2}{c|}{SC03}               & \multicolumn{2}{c|}{SC04}               \\ \cmidrule(l){2-9}
                            & $w_{1}$ best score & $w_{2}$ best score & $w_{1}$ best score & $w_{2}$ best score & $w_{1}$ best score & $w_{2}$ best score & $w_{1}$ best score & $w_{2}$ best score \\ \midrule
1                           & 18.886             & 45.581             & 38.004             & 53.950             & 65.869             & 70.016             & 1.589              & 50.585             \\ \midrule
50                          & -46.552            & 12.097             & -7.883             & 39.549             & 4.234              & 16.005             & -269.252           & 9.247              \\ \midrule
100                         & -46.580            & 12.097             & -8.467             & 37.845             & 4.217              & 15.849             & -281.689           & 9.245              \\ \midrule
150                         & -46.584            & 12.097             & -8.888             & 37.683             & 4.213              & 15.689             & -284.571           & 9.245              \\ \midrule
200                         & -46.591            & 12.097             & -8.933             & 37.544             & 4.211              & 15.507             & -284.797           & 9.245              \\ \bottomrule
\end{tabular}
\label{tab:GA-generations}
\end{table*}

\begin{figure*}[!htb]
    \centering
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC01-1-1-250.png}
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC01-1-1-25.png}
    \caption{Genetic Algorithm solutions for scenario \textbf{SC01}. On the left side using the function $w_{1}$ and on the right side using the function $w_{2}$.}
    \label{tab:result-GA-1}
\end{figure*}

Figure~\ref{tab:result-GA-1} shows the solutions obtained for scenario \textbf{SC01}, and in this scenario we can already perceive a difference between the solutions. While the solution that uses function $w_{1}$ tries to keep as much distance as possible by avoiding obstacles, the solution using function $w_{2}$ seems to be more interested in making the journey with less rotations and with the shortest distance.

\begin{figure*}[!htb]
    \centering
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC02-1-1-25.png}
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC02-1-1-250.png}
    \caption{Genetic Algorithm solutions for scenario \textbf{SC02}. On the left side using the function $w_{1}$ and on the right side using the function $w_{2}$.}
    \label{tab:result-GA-2}
\end{figure*}

Figure~\ref{tab:result-GA-2} shows the solutions obtained for scenario \textbf{SC02}, we can see that there was no significant change in the path (probably because of the scenario characteristic), but the solution using function $w_{1}$ seems to be more smooth.

\begin{figure*}[!htb]
    \centering
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC03-1-1-250.png}
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC03-1-1-25.png}
    \caption{Genetic Algorithm solutions for scenario \textbf{SC03}. On the left side using the function $w_{1}$ and on the right side using the function $w_{2}$.}
    \label{tab:result-GA-3}
\end{figure*}

Figure~\ref{tab:result-GA-3} shows the solutions obtained for scenario \textbf{SC03}, we can see that both solutions are smooth paths and without abrupt curves, but the solution using the $w_{2}$ function tends to choose the shortest path, this is more evident at the end of the path.

\begin{figure*}[!htb]
    \centering
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC04-1-1-250.png}
    \includegraphics[width=0.37\textwidth]{figuras/GA/SC04-1-1-25.png}
    \caption{Genetic Algorithm solutions for scenario \textbf{SC04}. On the left side using the function $w_{1}$ and on the right side using the function $w_{2}$.}
    \label{tab:result-GA-4}
\end{figure*}

Figure~\ref{tab:result-GA-4} shows the solutions obtained for scenario \textbf{SC04}, in this scenario we can clearly see that weight function made a total difference in the solution. The solution using the function $w_{1}$ prefers to follow an extremely long path, but with a good distance from the obstacles, while the solution using the function $w_{2}$ follows almost straight and passing very close of two obstacles.

