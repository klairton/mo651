The Genetic Algorithm metaheuristic is widely used on optimization problems~\cite{jog1991parallel} and has already been used for path planning optimization~\cite{lamini2018genetic, patle2018matrix}. A genetic algorithm is a bio-inspired search heuristic that is based on the theory of evolution. It uses the concepts such as generations, population, mutations, inheritance of parents characteristics, and selection of the fittest individuals for reproduction.

In this context, an individual represents a solution (path), a population is a set of individuals, and a generation is an iteration of the algorithm. Figure~\ref{fig:flow_genetic} presents a flowchart of the genetic algorithm. The phases of the algorithm are detailed next.

\begin{figure}[tb]
  \center
  \includegraphics[width=0.8\linewidth]{figuras/GA.png}
  \caption{Flowchart of the Genetic Algorithm.\label{fig:flow_genetic}}
\end{figure}


\begin{itemize}
  \item Initial Population: $0.9 \times$ INITIAL\_POPULATION random solutions are created using the GridSearch and $0.1 \times$ INITIAL\_POPULATION random solutions are created using the ProbabilisticRoadmap.
  \item Fitness Function: each individual receives a score based on a fitness function (Genetic algorithm terminology for objective function). The fitness function assigns a score to an individual $p$ as follows: considering the weight function $w(P) = (\alpha,\beta,\gamma)$, the score is given by $score = (\alpha \times distance(P)) + (\beta \times smoothness(P)) - (\gamma \times safety)$.
  \item Selection: at this stage, the top SELECTION individuals (ordered by their score with ties decided arbitrarily) are selected to the next generation.
  \item Crossover: at this phase, the population has the individuals selected in the Selection phase. To perform the re-population, individuals are randomly paired for crossovers. The crossover is executed SELECTION times. For each iteration, there is a probability equal to the parameter CROSSOVER for the crossover to be performed and, if so, a pair of random individuals is randomly chosen. Given two individuals with paths A and B, a new individual is generated as follows: A point $x$ on path A is randomly chosen, then a point $y$ on path B is chosen so that $y$ is the point on path B closest to $x$ and does not cause a collision. The new path is formed by points A[...,x] + B[y,...] (Figure~\ref{fig:ga-op} a)). If possible, this process generates two new paths, one choosing a random point from path A and the other choosing a random point from path B.
  \item Mutation: For each individual of the current population, a mutation occurs with probability equal to the parameter MUTATION. If an individual is selected for mutation, the following procedure is applied to create a new individual: Randomly two consecutive points $x$ and $y$ are chosen from the path. Then, the middle point $z$ between $x$ and $y$ is computed. Within a radius of up to 1 meter from point $z$ a new point $z^\prime$ is chosen, if this new point $z^\prime$ does not collide with any obstacle it will be incorporated into the path between the $x$ and $y$ points of the new individual. (Figure~\ref{fig:ga-op} b)).
  \item Smooth: For each individual of the current population, the procedure Smooth is applied to this individual with probability equal to the parameter SMOOTH. In this process is applied a gaussian filter that tries to smooth the path, this operator always returns a new path with 50 points based on the points initially provided. The proportion of smoothing on the path is given by a $\sigma$ factor that is randomly chosen between 0.5 and 2.0 (Illustration with reduced points in Figure~\ref{fig:ga-op} c)).
  \item Path Compression: For each point $x$ in the path of an individual of the population after the selection procedure, with probability equal to the parameter PATH\_COMPRESSION this procedure is applied. For all later points in the path of an individual, it is verified if there is a path that connects to $x$ without causing a collision. In the end, the point $y$ that is farthest from $x$ following the path order and that satisfies the non-collision condition is selected. The path is updated to P[...,x,y,...] and the procedure is repeated with the next points of the path with probability equal to the parameter PATH\_COMPRESSION. A new individual is created considering this updated path (Figure~\ref{fig:ga-op} d)).
  \item Avoid Collision: For each individual of the current population, the procedure Avoid Collision is applied to this individual with probability equal to the parameter AVOID\_COLLISION. This produces a new individual for each pair of consecutive points x and y in which a collision is detected, and it is done a new attempt to insert a new point between them similar to that described in the mutation procedure. The difference between this procedure and the mutation is that this strategy is applied to every pair of consecutive points on the path where a collision is detected (Figure~\ref{fig:ga-op} e)).
\end{itemize}

\begin{figure}[tb]
  \center
  \includegraphics[width=\linewidth]{figuras/GA-OP.png}
  \caption{Genetic Algorithm operators.\label{fig:ga-op}}
\end{figure}


The inputs of this algorithm are the set of obstacles, the origin point A, the target point B, and the parameters: INITIAL\_POPULATION, the initial population size; SELECTION the number of individuals selected in a given population; GENERATIONS, number of generations created by the algorithm; CROSSOVER, the probability that a pair of individuals generates a new individual; MUTATION, the probability that an individual is selected for mutation; SMOOTH, probability that the procedure that smooth the path is executed on an individual; PATH\_COMPRESSION, probability that the procedure that compress paths is used on an individual; AVOID\_COLLISION: probability that the procedure that tries to turn an unfeasible solution to a feasible solution is executed on an individual.
