import sys, time
import math
sys.path.insert(0, '../lib')
import vrep

class Robot():
	def __init__(self):
		self.ROBOT_WIDTH = 0.381
		self.WHEEL_RADIUS = 0.195/2.0
		self.SERVER_IP = "127.0.0.1"
		self.SERVER_PORT = 25000
		self.client_id = self.startSim()
		self.us_handle, self.vision_handle, self.laser_handle, self.gyro_handle = self.startSensors()
		self.motors_handle = self.startMotors()
		self.robot_handle = self.startRobot()

	def startSim(self):
		"""
			Function  to start the simulation. The scene must be running before running this code.
		    Returns:
		        client_id: This ID is used to start the objects on the scene.
		"""
		vrep.simxFinish(-1)
		client_id = vrep.simxStart(self.SERVER_IP, self.SERVER_PORT, True, True, 2000, 5)
		if client_id != -1:
			print("Connected to remoteApi server.")
		else:
			vrep.simxFinish(client_id)
			sys.exit("\033[91m ERROR: Unable to connect to remoteApi server. Consider running scene before executing script.")

		return client_id

	def startSensors(self):
		"""
			Function to start the sensors.
		    Returns:
		        us_handle: List that contains each ultrassonic sensor handle ID.
				vision_handle: Contains the vision sensor handle ID.
				laser_handle: Contains the laser handle ID.
		"""
		#Starting ultrassonic sensors
		us_handle = []
		sensor_name=[]
		for i in range(0,16):
			sensor_name.append("Pioneer_p3dx_ultrasonicSensor" + str(i+1))

			res, handle = vrep.simxGetObjectHandle(self.client_id, sensor_name[i], vrep.simx_opmode_oneshot_wait)
			if(res != vrep.simx_return_ok):
				print ("\033[93m "+ sensor_name[i] + " not connected.")
			else:
				print ("\033[92m "+ sensor_name[i] + " connected.")
				us_handle.append(handle)

		#Starting vision sensor
		res, vision_handle = vrep.simxGetObjectHandle(self.client_id, "Vision_sensor", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			print ("\033[93m Vision sensor not connected.")
		else:
			print ("\033[92m Vision sensor connected.")

		#Starting laser sensor
		res, laser_handle = vrep.simxGetObjectHandle(self.client_id, "fastHokuyo", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			print ("\033[93m Laser not connected.")
		else:
			print ("\033[92m Laser connected.")

		#Starting gyroscope sensor
		res, gyro_handle = vrep.simxGetObjectHandle(self.client_id, "GyroSensor", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			print ("\033[93m Gyroscope not connected.")
		else:
			print ("\033[92m Gyroscope connected.")

		return us_handle, vision_handle, laser_handle, gyro_handle

	def startMotors(self):
		"""
			Function to start the motors.
		    Returns:
		        A dictionary that contains both motors handle ID.
		"""

		res, left_handle = vrep.simxGetObjectHandle(self.client_id, "Pioneer_p3dx_leftMotor", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			print("\033[93m Left motor not connected.")
		else:
			print("\033[92m Left motor connected.")

		res, right_handle = vrep.simxGetObjectHandle(self.client_id, "Pioneer_p3dx_rightMotor", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			print("\033[93m Right motor not connected.")
		else:
			print("\033[92m Right motor connected.")

		return {"left": left_handle, "right":right_handle}

	def startRobot(self):
		"""
			Function to start the robot.
			Returns:
				robot_handle: Contains the robot handle ID.
		"""
		res, robot_handle = vrep.simxGetObjectHandle(self.client_id, "Pioneer_p3dx", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			print("\033[93m Robot not connected.")
		else:
			print("\033[92m Robot connected.")

		return robot_handle

	def getCurrentPosition(self):
		"""
			Gives the current robot position on the environment.
			Returns:
				position: Array with the (x,y,z) coordinates.
		"""
		res, position = vrep.simxGetObjectPosition(self.client_id, self.robot_handle, -1, vrep.simx_opmode_streaming)
		while(res != vrep.simx_return_ok):
			res, position = vrep.simxGetObjectPosition(self.client_id, self.robot_handle, -1, vrep.simx_opmode_streaming)

		return position

	def getCurrentOrientation(self):
		"""
			Gives the current robot orientation on the environment.
			Returns:
				orientation: Array with the euler angles (alpha, beta and gamma).
		"""
		res, orientation = vrep.simxGetObjectOrientation(self.client_id, self.robot_handle, -1, vrep.simx_opmode_streaming)
		while(res != vrep.simx_return_ok):
			res, orientation = vrep.simxGetObjectOrientation(self.client_id, self.robot_handle, -1, vrep.simx_opmode_streaming)

		return orientation

	def getWheelsAngles(self):
		thetaL = vrep.simxGetJointPosition(self.client_id, self.motors_handle["left"], vrep.simx_opmode_streaming)[1]
		thetaR = vrep.simxGetJointPosition(self.client_id, self.motors_handle["right"], vrep.simx_opmode_streaming)[1]
		return thetaL, thetaR

	def getConnectionStatus(self):
		"""
			Function to inform if the connection with the server is active.
			Returns:
				connectionId: -1 if the client is not connected to the server.
				Different connection IDs indicate temporary disconections in-between.
		"""
		return vrep.simxGetConnectionId(self.client_id)

	def isConnected(self):
		return vrep.simxGetConnectionId(self.client_id) != -1

	def readUltrassonicSensors(self, noDetectionDist = 2.0):
		"""
			Reads the distances from the 16 ultrassonic sensors.
			Returns:
				distances: List with the distances in meters.
		"""
		distances = []

		for sensor in self.us_handle:
			res, status, distance,_,_ = vrep.simxReadProximitySensor(self.client_id, sensor, vrep.simx_opmode_streaming)
			while(res != vrep.simx_return_ok):
				res, status, distance,_,_ = vrep.simxReadProximitySensor(self.client_id, sensor, vrep.simx_opmode_buffer)

			if(status != 0):
				distances.append(min(noDetectionDist, distance[2]))
			else:
				distances.append(noDetectionDist)


		return distances

	def readVisionSensor(self):
		"""
			Reads the image raw data from vrep vision sensor.
			Returns:
				resolution: Tuple with the image resolution.
				image: List with the image data.
		"""
		res, resolution, image = vrep.simxGetVisionSensorImage(self.client_id, self.vision_handle, 0, vrep.simx_opmode_streaming)
		while(res != vrep.simx_return_ok):
			res, resolution, image = vrep.simxGetVisionSensorImage(self.client_id, self.vision_handle, 0, vrep.simx_opmode_buffer)

		return resolution, image

	def readGyroSensor(self):
		"""
			Reads the robot gyroscope orientation.
			Returns:
				orientation: Array with the euler angles (alpha, beta and gamma).
		"""
		res, orientation = vrep.simxGetObjectOrientation(self.client_id, self.gyro_handle, -1, vrep.simx_opmode_streaming)
		while(res != vrep.simx_return_ok):
			res, orientation = vrep.simxGetObjectOrientation(self.client_id, self.gyro_handle, -1, vrep.simx_opmode_streaming)

		return orientation

	def readLaserSensor(self):
		"""
			Gets the 572 points read by the laser sensor. Each reading contains 3 values (x, y, z) of the point relative to the sensor position.
			Returns:
				laser: List with 1716 values of x, y and z from each point.
		"""
		res, laser = vrep.simxGetStringSignal(self.client_id,"LasermeasuredDataAtThisTime", vrep.simx_opmode_streaming)
		laser = vrep.simxUnpackFloats(laser)
		while(res != vrep.simx_return_ok):
			res, laser = vrep.simxGetStringSignal(self.client_id,"LasermeasuredDataAtThisTime", vrep.simx_opmode_buffer)
			laser = vrep.simxUnpackFloats(laser)

		return laser

	def stop(self):
		"""
			Sets the motors velocities to 0.
		"""
		vrep.simxSetJointTargetVelocity(self.client_id, self.motors_handle["left"], 0, vrep.simx_opmode_streaming)
		vrep.simxSetJointTargetVelocity(self.client_id, self.motors_handle["right"], 0, vrep.simx_opmode_streaming)
		time.sleep(0.1)

	def setLeftVelocity(self, vel):
		"""
			Sets the velocity on the left motor.
			Args:
				vel: The velocity to be applied in the motor (rad/s)
		"""
		vrep.simxSetJointTargetVelocity(self.client_id, self.motors_handle["left"], vel, vrep.simx_opmode_streaming)

	def setRightVelocity(self, vel):
		"""
			Sets the velocity on the right motor.
			Args:
				vel: The velocity to be applied in the motor (rad/s)
		"""
		vrep.simxSetJointTargetVelocity(self.client_id, self.motors_handle["right"], vel, vrep.simx_opmode_streaming)

	def setVelocity(self, V, W):
		"""
			Sets a linear and a angular velocity on the robot.
			Args:
				V: Linear velocity (m/s) to be applied on the robot along its longitudinal axis.
				W: Angular velocity (rad/s) to be applied on the robot along its axis of rotation, positive in the counter-clockwise direction.
		"""
		left_vel = (V - W*(self.ROBOT_WIDTH/2))/self.WHEEL_RADIUS
		right_vel = (V + W*(self.ROBOT_WIDTH/2))/self.WHEEL_RADIUS
		vrep.simxSetJointTargetVelocity(self.client_id, self.motors_handle["left"], left_vel, vrep.simx_opmode_streaming)
		vrep.simxSetJointTargetVelocity(self.client_id, self.motors_handle["right"], right_vel, vrep.simx_opmode_streaming)


