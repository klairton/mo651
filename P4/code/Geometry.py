import math
import random

def grid_point(a, r):
  x1, y1 = a
  x2, y2 = (random.uniform(-r, r), random.uniform(-r, r))
  return (x1 + x2, y1 + y2)

def mid_point(a, b):
  x1, y1 = a
  x2, y2 = b
  return ((x1 + x2) / 2.0, (y1 + y2) / 2.0)

def angle(a, b):
  x1, y1 = a
  x2, y2 = b
  return ((math.atan2(y1 - y2, x1 - x2) + (2 * math.pi)) % (2 * math.pi))

def distance(a, b):
  x1, y1 = a
  x2, y2 = b
  return (((x2 - x1) ** 2) + ((y2 - y1) ** 2)) ** (1.0 / 2.0)

def line_point(a, b, c):
  # get distance from the point to the two ends of the line
  d1 = distance(c, a)
  d2 = distance(c, b)

  # get the length of the line
  line_length = distance(a, b)

  # since floats are so minutely accurate, add
  #  a little buffer zone that will give collision

  # if the two distances are equal to the line's
  # length, the point is on the line!
  # note we use the buffer here to give a range,
  # rather than one 
  return abs((d1 + d2) - line_length) <= 0.01

def point_circle(a, c, r):
  # get distance between the point and circle's center
  # using the Pythagorean Theorem
  # if the distance is less than the circle's
  # radius the point is inside!
  if distance(a, c) <= r:
    return True
  return False

def line_circle(a, b, c, r):
  x1, y1 = a
  x2, y2 = b
  cx, cy = c
  # is either end INSIDE the circle?
  # if so, return true immediately
  if point_circle(a, c, r) or point_circle(b, c, r):
    return True

  # get length of the line
  length = max(distance(a, b), 0.00001) #prevents division by zero

  # get dot product of the line and circle
  dot = 0
  try:
    dot = ( ((cx - x1) * (x2 - x1)) + ((cy - y1) * (y2 - y1)) ) / (length ** 2)
  except ZeroDivisionError:
    return False

  # find the closest point on the line
  closest_x = x1 + (dot * (x2 - x1))
  closest_y = y1 + (dot * (y2 - y1))

  # is this point actually on the line segment?
  # if so keep going, but if not, return false
  if not line_point(a, b, (closest_x, closest_y)):
    return False
  
  # get distance to closest point
  if distance((closest_x, closest_y), c) <= r:
    return True
  return False

def distance_line_circle(a, b, c, r):
  x1, y1 = a
  x2, y2 = b
  cx, cy = c
  # is either end INSIDE the circle?
  # if so, return true immediately
  if point_circle(a, c, r) or point_circle(b, c, r):
    return min(distance(a, c), distance(b, c)) - r

  # get length of the line
  length = distance(a, b)

  # get dot product of the line and circle
  dot = 0
  try:
    dot = ( ((cx - x1) * (x2 - x1)) + ((cy - y1) * (y2 - y1)) ) / (length ** 2)
  except ZeroDivisionError:
    return distance(a, c) - r

  # find the closest point on the line
  closest_x = x1 + (dot * (x2 - x1))
  closest_y = y1 + (dot * (y2 - y1))

  # is this point actually on the line segment?
  # if so keep going, but if not, return false
  if not line_point(a, b, (closest_x, closest_y)):
    return min(distance(a, c), distance(b, c)) - r
  else:
    return distance((closest_x, closest_y), c) - r