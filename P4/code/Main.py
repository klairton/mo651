from __future__ import print_function
import sys, time
import matplotlib.pyplot as plt
from GridMap import GridMap
from PathSmoothing import *

sys.path.insert(0, '../src')
from Robot import Robot
from GoToGoalPIDaux import GoToGoalPID

path = [(1.0, 9.0), (1.0, 8.5), (1.0, 8.0), (1.0, 7.5), (1.0, 7.0), (1.5, 6.5), (2.0, 6.0), (2.5, 6.0), (3.0, 5.5), (3.5, 5.0), (4.0, 4.5), (4.5, 4.0), (5.0, 4.0), (5.5, 3.5), (6.0, 3.0), (6.0, 2.5), (6.5, 2.0), (6.5, 1.5), (7.0, 1.0), (7.5, 0.5), (8.0, 0.5), (8.5, 0.5), (9.0, 1.0)]
obstacles = []

# Scene 01 Obstacles
obstacles = [((2.0,8.0), 0.5), ((5.0,8.0), 0.5), ((8.0,8.0), 0.5), ((8.0,5.0), 0.5), ((5.0,5.0), 0.5), ((2.0,5.0), 0.5), ((2.0,2.0), 0.5), ((5.0,2.0), 0.5), ((8.0,2.0), 0.5)]

# Scene 02 Obstacles
# obstacles = [
#   ((0.5,8.0), 0.5), ((1.5,8.0), 0.5), ((2.5,8.0), 0.5), ((3.5,8.0), 0.5), ((4.5,8.0), 0.5), ((5.5,8.0), 0.5), ((6.5,8.0), 0.5), ((7.5,8.0), 0.5),
#   ((2.5,5.0), 0.5), ((3.5,5.0), 0.5), ((4.5,5.0), 0.5), ((5.5,5.0), 0.5), ((6.5,5.0), 0.5), ((7.5,5.0), 0.5), ((8.5,5.0), 0.5), ((9.5,5.0), 0.5),
#   ((0.5,2.0), 0.5), ((1.5,2.0), 0.5), ((2.5,2.0), 0.5), ((3.5,2.0), 0.5), ((4.5,2.0), 0.5), ((5.5,2.0), 0.5), ((6.5,2.0), 0.5), ((7.5,2.0), 0.5),
# ]

# Scene 03 Obstacles
# obstacles = [
#   ((1.5,7.5), 0.5), ((1.5,5.5), 0.5), ((1.5,3.5), 0.5), ((1.5,1.5), 0.5),
#   ((5.0,8.0), 2.0), ((5.0,2.5), 2.5),
#   ((8.5,8.5), 0.5), ((8.5,6.5), 0.5), ((8.5,4.5), 0.5), ((8.5,2.5), 0.5),
# ]

# Scene 04 Obstacles
# obstacles = [((5.0,0.5), 0.5), ((5.0,1.5), 0.5), ((5.0,3.5), 0.5), ((5.0,4.5), 0.5), ((5.0,5.5), 0.5)]

circles = []
for p, r in obstacles:
  circles.append(plt.Circle(p, r, color='orange'))

fig, ax = plt.subplots(figsize=(5, 5))
ax.set(xlim=(0.0, 10.0), ylim=(0.0, 10.0))

for c in circles:
  ax.add_artist(c)

X = []
Y = []

for x, y in path:
  X.append(x)
  Y.append(y)

plt.plot(X, Y, "-b")
plt.show()

robot = Robot()
go_to_goal = GoToGoalPID()

for i in range(len(X)):
  x, y = X[i], Y[i]
  print("Next Goal:", x, y)
  print()
  c_x, c_y, _ = robot.getCurrentPosition()
  distance = ((x - c_x) ** 2 + (y - c_y) ** 2) ** (1.0 / 2.0)

  while distance > 0.5:
    try:
      v_left, v_right = go_to_goal.go(robot, (x, y))
      robot.setLeftVelocity(v_left)
      robot.setRightVelocity(v_right)
      time.sleep(0.1)
      c_x, c_y, _ = robot.getCurrentPosition()
      distance = ((x - c_x) ** 2.0 + (y - c_y) ** 2.0) ** (1.0 / 2.0)
    except:
      break
  go_to_goal.reset()

robot.stop()
print("Simulation Finished.")
