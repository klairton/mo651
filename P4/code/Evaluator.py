import Geometry
import math

class Evaluator():
  def __init__(self, _walls, _obstacles):
    # Robot radius
    self.r_radius = 0.4
    # The set of continuous points (x,y) that delimit an environment
    self.walls = _walls[:]
    # The set of positions and radius ((x,y), r) of spheres in the environment
    self.obstacles = _obstacles[:]

  def freeSpace(self, p):
    x, y = p
    if x >= 10 or x <= 0 or y >= 10 or y <= 0:
      return False
    for c, r in self.obstacles:
      if Geometry.point_circle(p, c, r + self.r_radius):
        return False
    for i in range(len(self.walls)):
      a = self.walls[i % len(self.walls)]
      b = self.walls[(i + 1) % len(self.walls)]
      if Geometry.line_circle(a, b, p, self.r_radius):
        return False
    return True

  def collision(self, a, b):
    for c, r in self.obstacles:
      if Geometry.line_circle(a, b, c, r + self.r_radius):
        return True
    return False

  def evaluate(self, individial, debug = False):
    # Default individual initialization
    individial.feasible = True
    individial.distance = 0.0
    individial.safety = 10.0
    individial.smoothness = 0.0
    individial.collisions = 0

    # Full path include the start and target points
    full_path = individial.fullPath()

    # Smoothness considering 3 consecutive points
    # Initial angle is always 0 in our scenarios
    b = full_path[0]
    c = full_path[1]
    alpha = 0.0
    beta = ((Geometry.angle(b, c) + 2 * math.pi) % (2 *math.pi)) * (180.0 / math.pi)
    angle = (min(abs(alpha - beta), (360.0 - abs(alpha - beta))) / 60.0) ** (2.0)
    individial.smoothness += angle
    # Smoothness for the path
    x = 0
    while x < len(full_path) - 2:
      a = full_path[x]
      b = full_path[x + 1]
      c = full_path[x + 2]
      alpha = ((Geometry.angle(a, b) + 2 * math.pi) % (2 *math.pi)) * (180.0 / math.pi)
      beta = ((Geometry.angle(b, c) + 2 * math.pi) % (2 *math.pi)) * (180.0 / math.pi)
      angle = (min(abs(alpha - beta), (360.0 - abs(alpha - beta))) / 60.0) ** (2.0)
      individial.smoothness += angle
      x += 1

    # Collision test and checking the distance to the obstacles
    for i in range(len(full_path) - 1):
      a = full_path[i]
      b = full_path[i + 1]

      # Path distance updated: the length of the segment (x1, y1), (x2, y2) is added up
      individial.distance += Geometry.distance(a, b)

      for c, r in self.obstacles:
        if debug:
          print(a, b, c)
          print(Geometry.line_circle(a, b, c, r + self.r_radius))
        if Geometry.line_circle(a, b, c, r + self.r_radius):
          individial.feasible = False
          individial.collisions += 1
        distance = Geometry.distance_line_circle(a, b, c, r + self.r_radius)
        if distance < individial.safety:
          individial.safety = distance

    # Collision test and checking the distance to the walls
    for i in range(len(self.walls)):
      a = self.walls[i % len(self.walls)]
      b = self.walls[(i + 1) % len(self.walls)]

      for p in full_path:
        x, y = p
        if x >= 10 or x <= 0 or y >= 10 or y <= 0:
          individial.feasible = False
          individial.collisions += 1
        if Geometry.line_circle(a, b, p, self.r_radius):
          individial.feasible = False
          individial.collisions += 1
        distance = Geometry.distance_line_circle(a, b, p, self.r_radius)
        if distance < individial.safety:
          individial.safety = distance
