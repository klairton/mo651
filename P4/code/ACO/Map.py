#!/usr/bin/env python
import sys
sys.path.insert(1, '../')

import numpy as np
import matplotlib.pyplot as plt
from GridMap import GridMap
from Evaluator import Evaluator

class Map:
    ''' Class used for handling the
        information provided by the
        input map '''
    class Nodes:
        ''' Class for representing the nodes
            used by the ACO algorithm '''
        def __init__(self, row, col, in_map,spec):
            self.node_pos= (row, col)
            self.edges = self.compute_edges(in_map)
            self.spec = spec

        def compute_edges(self,map_arr):
            ''' class that returns the edges
                connected to each node '''
            imax = map_arr.shape[0]
            jmax = map_arr.shape[1]
            edges = []
            if map_arr[self.node_pos[0]][self.node_pos[1]] == 1:
                for dj in [-1,0,1]:
                    for di in [-1,0,1]:
                        newi = self.node_pos[0]+ di
                        newj = self.node_pos[1]+ dj
                        if ( dj == 0 and di == 0):
                            continue
                        if (newj>=0 and newj<jmax and newi >=0 and newi<imax):
                            if map_arr[newi][newj] == 1:
                                edges.append({'FinalNode':(newi,newj),
                                              'Pheromone': 0.1, 'Probability':
                                             0.0, 'Updated': False})
            return edges

    def __init__(self, walls, obstacles, start, target, _GRID_SIZE = 10, margin = 0.0):
        _obstacles = []
        for point, radius in obstacles:
            _obstacles.append((point, radius + margin))

        grid_map = GridMap(walls, _obstacles, _GRID_SIZE = _GRID_SIZE)
        evaluator = Evaluator(walls, _obstacles)
        grid, occupancy_map = grid_map.getGridOccupance()
        start, target = grid_map.gridChoose(grid, occupancy_map, start, target)
        self.grid = grid
        self.occupancy_map = np.copy(occupancy_map)
        self.initial_node = start
        self.final_node = target
        self.occupancy_map[self.initial_node[0]][self.initial_node[1]] = True
        self.occupancy_map[self.final_node[0]][self.final_node[1]] = True
        self.nodes_array = self._create_nodes()

    def _create_nodes(self):
        ''' Create nodes out of the initial map '''
        m_nodes = []
        for i in range(self.occupancy_map.shape[0]):
            l_nodes = []
            for j in range(self.occupancy_map.shape[0]):
                spec = 'O'
                if (i,j) == self.initial_node:
                    spec = 'S'
                elif (i,j) == self.final_node:
                    spec = 'F'
                elif self.occupancy_map[i][j]:
                    spec = 'E'
                node = self.Nodes(i,j,self.occupancy_map,spec)
                l_nodes.append(node)
            m_nodes.append(l_nodes)
        return m_nodes

    # def _map_2_occupancy_map(self):
    #     ''' Takes the matrix and converts it into a float array '''
    #     map_arr = np.copy(self.in_map)
    #     map_arr[map_arr == 'O'] = 0
    #     map_arr[map_arr == 'E'] = 1
    #     map_arr[map_arr == 'S'] = 1
    #     map_arr[map_arr == 'F'] = 1
    #     return map_arr.astype(np.int)

    def represent_map(self):
        ''' Represents the map '''
        # Map representation
        plt.plot(self.initial_node[1],self.initial_node[0], 'ro', markersize=10)
        plt.plot(self.final_node[1],self.final_node[0], 'bo', markersize=10)
        plt.imshow(self.occupancy_map, cmap='gray', interpolation = 'nearest')
        plt.show()
        plt.close()

    def get_path(self, path):
        solution = []
        for x,y in path:
            solution.append(self.grid[x][y])
        return solution
    def represent_path(self, path):
        ''' Represents the path in the map '''
        x = []
        y = []
        for p in path:
            x.append(p[1])
            y.append(p[0])
        plt.plot(x,y,lw=2)
        self.represent_map()
