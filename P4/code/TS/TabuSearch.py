from Moviment import Moviment
from Individual import Individual
import random
import sys
import math
import copy

sys.path.insert(1, '../')
from ProbabilisticRoadmap import run_roadmap
from Evaluator import Evaluator
from GridMap import GridMap

class TabuSearch():
  def __init__(self, _start, _target, _walls, _obstacles, tabu_size):
    self.start = _start
    self.target = _target
    self.evaluator = Evaluator(_walls, _obstacles)
    self.grid_map = GridMap(_walls, _obstacles)
    self.TL = [-1 for x in range(tabu_size)]
    self.PATH_COMPRESSION = 0.25
    self.walls = _walls
    self.obstacles = _obstacles
    #self.AVOID_COLLISION = 0.1
    self.INTERATIONS = 150
    self.best_solution = Individual(_start, _target)
    self.incumbent = self.best_solution
    self.MOVEX = 1
    self.MOVEY = 2
    self.BSWAP = 3
    self.ADD = 4
    self.REMOVE = 5
    self.MAX_X = -1
    self.MAX_Y = -1
    self.MIN_X = 0x3f3f3f3f
    self.MIN_Y = 0x3f3f3f3f
    self.Melhorou = 0
    self.freq = []
	
    for w in self.walls:
        self.MAX_X = max(w[0], self.MAX_X)
        self.MAX_Y = max(w[1], self.MAX_Y)
        self.MIN_X = min(w[0], self.MIN_X)
        self.MIN_Y = min(w[1], self.MIN_Y)





  def pathCompression(self):
    if random.uniform(0.0, 1.0) <= self.PATH_COMPRESSION:
      individual = self.incumbent
      full_path = individual.fullPath()
      x = 0
      while x < len(full_path) - 2:
        y = x + 2
        while y < len(full_path) and not self.evaluator.collision(full_path[x], full_path[y]):
          full_path = full_path[:(x + 1)] + full_path[y:]
        x += 1
      individual = Individual(self.start, self.target)
      individual.path = full_path[1:-1]
      
   
  def fitnessFuction(self, solution):
    i = solution
    #path = copy.copy(solution.path)
    self.evaluator.evaluate(i)
    #solution.path = path
    #i.score = (1.0 * i.distance) + (1.0 * i.smoothness) + (1.0 * i.energy) - (100.0 * i.safety) + (1000 * i.collisions)
    #i.score = (1.0 * i.distance) + (0.25 * i.smoothness) + (1.0 * i.energy) - (250.0 * i.safety) + (15000 * i.collisions)
    i.score = (1.0 * i.distance) + (i.smoothness / 90.0) - (250.0 * i.safety) + (1000 * i.collisions)
    #out = 0
    #for p in solution.path:
    #    if(p[0] < self.MIN_X or p[0] > self.MAX_X or p[1] < self.MIN_Y or p[1] > self.MAX_Y):
    #        i.feasible = 0
    #        out = 1
			
			
    #if (i.feasible == 0):
        #i.score += 2000
    #if (out):
    #    i.score += 10000
    return i.score


  def ConstructiveHeuristic(self):
   
    individual = Individual(self.start, self.target)
    individual.path = self.grid_map.run(self.start, self.target, 10 + random.randint(0, 10))
    self.incumbent = copy.copy(individual)
    self.fitnessFuction(self.incumbent)
	
    for i in range(200):
	    individual.path = self.grid_map.run(self.start, self.target, 10 + random.randint(0, 10))
	    if self.fitnessFuction(individual) < self.incumbent.score:
             self.incumbent = copy.copy(individual)
    
    self.freq = [0 for x in self.incumbent.path]
    return self.incumbent

  def Contains (self,moviment):
    return moviment in self.TL

  def AddTabu (self,moviment):
    self.TL = self.TL[1:]
    self.TL.append(moviment)
  

  def getBestX (self, indice):
    s = copy.copy(self.incumbent)
    best = copy.copy(self.incumbent)
    best.score = 0x3f3f3f3f
    if indice >= len(s.path):
        return
    bestpath = s.path[indice]
    ibest = -1
    for i in range (0, 21):
        if (self.Contains((self.MOVEX, indice, i))):
            continue
        x,y = s.path[indice]
        nx = min(x+(i/10),10)
        s.path[indice] = (nx, y)
        if (self.fitnessFuction(s) < best.score):
           best.score = s.score
           self.fitnessFuction(best)
           bestpath = (nx, y)
           ibest = copy.copy(i)
        nx = max(x-(i/10),0)
        s.path[indice] = (nx, y)
        if (self.fitnessFuction(s) < best.score):
           best.score = s.score
           self.fitnessFuction(best)
           bestpath = (nx, y)
           ibest = copy.copy(i)
        s.path[indice] = (x,y)
           
    
    #if (self.fitnessFuction(self.incumbent) > best.score):
    best.path[indice] = bestpath       
    self.incumbent = copy.copy(best)
    self.AddTabu((self.MOVEX, indice, ibest))
    
  def getBestY (self, indice):
    s = copy.copy(self.incumbent)
    best = copy.copy(self.incumbent)
    best.score = 0x3f3f3f3f
    bestpath = s.path[indice]
    ibest = -1
    for i in range (0, 21):
        if (self.Contains((self.MOVEY, indice, i))):
            continue
        x,y = s.path[indice]
        s.path[indice] = (x, y+(i/10))
        if (self.fitnessFuction(s) < best.score):
           best.score = s.score
           self.fitnessFuction(best)
           bestpath = (x, y+(i/10))
           ibest = copy.copy(i)
           
        s.path[indice] = (x, y-(i/10))
        if (self.fitnessFuction(s) < best.score):
           best.score = s.score
           self.fitnessFuction(best)
           bestpath = (x, y-(i/10))
           ibest = copy.copy(i)
           
        s.path[indice] = (x,y)
           
    
    #if (self.fitnessFuction(self.incumbent) > best.score):
    best.path[indice] = bestpath       
    self.incumbent = copy.copy(best)
    self.AddTabu((self.MOVEY, indice, ibest))
  
  
  def getBestSwap (self, indice):
    if len(self.incumbent.path) <= 1:
        return
    s = copy.copy(self.incumbent)
    best = copy.copy(self.incumbent)
    #best.score = 0x3f3f3f3f
    iBest = -1
    bestpath = copy.copy(self.incumbent.path)
    for i in range(len(s.path)):
        if i == indice or self.Contains((self.BSWAP, indice, i)):
            continue
        s.path[i], s.path[indice] = s.path[indice], s.path[i]
        if (self.fitnessFuction(s) < best.score):
            print("entrou!!\n")
            #print("best = ", s.path)
            best.score = s.score
            bestpath = copy.copy(s.path)
            ibest = copy.copy(i)
		
        s.path[i], s.path[indice] = s.path[indice], s.path[i]
		
    self.incumbent.path = bestpath
    self.AddTabu((self.BSWAP, indice, iBest))
    self.AddTabu((self.BSWAP, iBest, indice))
    
  def newPointInPath(self):
	  self.incumbent.path.append((0,0))
	  self.getBestX(len(self.incumbent.path)-1)
	  self.getBestY(len(self.incumbent.path)-1)
	  #self.getBestSwap(len(self.incumbent.path)-1)
  
  
  def removePointInPath(self, indice):
	  if (len(self.incumbent.path)>1):
	      self.incumbent.path.pop(indice)
  
  def NeighborhoodMove(self):
   self.Melhorou += 1
   if (len(self.incumbent.path) == 0):
        self.newPointInPath()
        
   moviment = random.randint(1,10)
   
   
   indices = [random.randint(0,max(len(self.incumbent.path)-1,0)) for x in range(5)]
   
   #indice = random.randint(0,max(len(self.incumbent.path)-1,0))
   for indice in indices:
       self.getBestX(indice)
       self.getBestY(indice)
       if (self.fitnessFuction(self.incumbent) < self.best_solution.score ):
           self.best_solution = copy.copy(self.incumbent)
       self.freq[indice] += 1
       #self.getBestSwap(indice)
   
#   if (len(self.incumbent.path) == 1):
#	   self.newPointInPath()
	
	
 #  elif (moviment == 10):
 #      self.removePointInPath(indice)
   
  # elif (moviment == 8 or moviment == 9):
	#   self.newPointInPath()
	
      
   #if (moviment >= 1 and moviment <= 8 and len(self.incumbent.path) > 1):

      
   
   
   
   #self.evaluator.evaluate(self.incumbent)
   if (self.fitnessFuction(self.incumbent) < self.best_solution.score ): # and isBetterThan best_solution
      self.best_solution = copy.copy(self.incumbent)
      #self.Melhorou = 0
   
   if (self.Melhorou%10 == 0):
	   menor = min(self.freq)
	   for i in range(0,len(self.freq)-1):
		   if self.freq[i] == menor:
			   self.getBestX(i)
			   self.getBestY(i)
			   if (self.fitnessFuction(self.incumbent) < self.best_solution.score ):
				   self.best_solution = copy.copy(self.incumbent)
			   self.freq[indice] += 1
			   self.getBestSwap(indice)
			   self.freq[i] += 1
        #for indice in range(len(self.incumbent.path)):
           #self.getBestSwap(len(self.incumbent.path)-indice-1)
           #self.getBestSwap(len(self.incumbent.path)-indice-1)
        #self.newPointInPath()
        #random.shuffle(self.incumbent.path)
   #if (self.Melhorou%20 == 0):
	   #self.removePointInPath(indice)
   print (len(self.incumbent.path), self.fitnessFuction(self.incumbent))


  def run(self):
    self.ConstructiveHeuristic()
    for i in range(self.INTERATIONS):
      print('\r', "Progress: {0} of {1} INTERATIONS {2}".format(i + 1, self.INTERATIONS, (i % 4) * "." + (4 - (i % 4)) * " "), end='')
      self.NeighborhoodMove()
      self.pathCompression()
  

    print('\r', "Status: Complete                    ")
    individual = self.best_solution
    print(" Feasible Solution: ", individual.feasible)
    print(" Path: ", self.best_solution.path)
    print(" Score: ", individual.score)
    print(" Distance: ", individual.distance)
    print(" Improved Individual: ", individual.new)
    



