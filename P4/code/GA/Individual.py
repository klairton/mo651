
class Individual():
  def __init__(self, _start, _target):
    self.start = _start
    self.target = _target
    self.path = []
    self.distance = 0
    self.smoothness = 0
    self.safety = 0
    self.collisions = 0
    self.score = 0
    self.feasible = False
    self.new = True

  def fullPath(self):
    return [self.start] + self.path + [self.target]