import random
import sys
import math

sys.path.insert(1, '../')
from ProbabilisticRoadmap import run_roadmap
from PathSmoothing import *
import Geometry
from Individual import Individual
from Evaluator import Evaluator
from GridMap import GridMap

class GeneticAlgorithm():
  def __init__(self, _start, _target, _walls, _obstacles):
    self.start = _start
    self.target = _target
    self.walls = _walls
    self.obstacles = _obstacles
    self.evaluator = Evaluator(_walls, _obstacles)
    self.grid_map = GridMap(_walls, _obstacles)
    self.population = []
    self.INITIAL_POPULATION = 100
    self.SELECTION = 50
    self.CROSSOVER = 0.5
    self.MUTATION = 0.5
    self.SMOOTH = 0.1
    self.PATH_COMPRESSION = 0.5
    self.AVOID_COLLISION = 0.25
    self.GENERATIONS = 100

  def initialPopulation(self):
    for _ in range(round(self.INITIAL_POPULATION * 0.9)):
      individual = Individual(self.start, self.target)
      individual.path = self.grid_map.run(self.start, self.target, 10 + random.randint(0, 10))
      individual.new = False
      self.population.append(individual)

    solutions = run_roadmap(self.start, self.target, self.walls, self.obstacles, grid_size = 50, solutions = round(self.INITIAL_POPULATION * 0.1), max_iter = 10)
    for solution in solutions:
      individual = Individual(self.start, self.target)
      individual.path = solution
      individual.new = False
      self.population.append(individual)

  def fitnessFuction(self):
    for i in self.population:
      self.evaluator.evaluate(i)
      i.score = (1.0 * i.distance) + (1.0 * i.smoothness) - (25 * i.safety) + (1000 * i.collisions)

  def selection(self):
    self.population.sort(key=lambda x: x.score)
    self.population = self.population[:self.SELECTION]

  def pathCompression(self):
    for i in range(self.SELECTION):
      individual = self.population[i]
      full_path = individual.fullPath()
      x = 0
      while x < len(full_path) - 2:
        y = x + 2
        z = x + 1
        if random.uniform(0.0, 1.0) <= self.PATH_COMPRESSION:
          while y < len(full_path):
            if not self.evaluator.collision(full_path[x], full_path[y]):
                z = y
            y += 1
        full_path = full_path[:(x + 1)] + full_path[z:]
        x += 1
      if len(full_path) < len(individual.fullPath()):
        individual = Individual(self.start, self.target)
        individual.path = full_path[1:-1]
        self.population.append(individual)

  def avoidCollision(self):
    for i in range(self.SELECTION):
      if random.uniform(0.0, 1.0) <= self.AVOID_COLLISION and self.population[i].collisions > 0:
        full_path = self.population[i].fullPath()
        x = 0
        while x < len(full_path) - 1:
          if self.evaluator.collision(full_path[x], full_path[x + 1]):
            m = Geometry.mid_point(full_path[x], full_path[x + 1])
            c = Geometry.grid_point(m, 1.0)
            t = 0
            while (not self.evaluator.freeSpace(c) or self.evaluator.collision(full_path[x], c) or self.evaluator.collision(c, full_path[x + 1])) and t < 100:
              c = Geometry.grid_point(m, 1.0)
              t += 1
            if self.evaluator.freeSpace(c) and not self.evaluator.collision(full_path[x], c) and not self.evaluator.collision(c, full_path[x + 1]):
              full_path.insert(x + 1, c)
              x += 1
          x += 1
        if len(full_path) > len(self.population[i].fullPath()):
          individual = Individual(self.start, self.target)
          individual.path = full_path[1:-1]
          self.population.append(individual)

  def crossover(self):
    for _ in range(self.SELECTION):
      if random.uniform(0.0, 1.0) <= self.CROSSOVER:
        x, y = random.sample(range(self.SELECTION), 2)
        father = self.population[x]
        mother = self.population[y]
        if len(father.path) > 0 and len(mother.path) > 0:
          a = random.randint(0, len(father.path) - 1)
          p = None
          distance = None
          for b in range(len(mother.path)):
            if not self.evaluator.collision(father.path[a], mother.path[b]):
              d = Geometry.distance(father.path[a], mother.path[b])
              if distance == None or d < distance:
                distance = d
                p = b
          if p != None:
            individual = Individual(self.start, self.target)
            individual.path = father.path[:a + 1] + mother.path[p:]
            self.population.append(individual)

          a = random.randint(0, len(mother.path) - 1)
          p = None
          distance = None
          for b in range(len(father.path)):
            if not self.evaluator.collision(mother.path[a], father.path[b]):
              d = Geometry.distance(mother.path[a], father.path[b])
              if distance == None or d < distance:
                distance = d
                p = b
          if p != None:
            individual = Individual(self.start, self.target)
            individual.path = father.path[:a + 1] + mother.path[p:]
            self.population.append(individual)

  def mutation(self):
    for i in range(self.SELECTION):
      if random.uniform(0.0, 1.0) <= self.MUTATION and len(self.population[i].path) > 1:
        individual = self.population[i]
        a = random.randint(0, len(individual.path) - 2)
        b = a + 1
        m = Geometry.mid_point(individual.path[a], individual.path[b])
        c = Geometry.grid_point(m, 1.0)
        t = 0
        while not self.evaluator.freeSpace(c) and t < 100:
          c = Geometry.grid_point(m, 1.0)
          t += 1
        if self.evaluator.freeSpace(c):
          individual = Individual(self.start, self.target)
          individual.path = self.population[i].path[:]
          individual.path.insert(a + 1, c)
          self.population.append(individual)

  def smooth(self):
    for i in range(self.SELECTION):
      if random.uniform(0.0, 1.0) <= self.SMOOTH and len(self.population[i].path) > 1:
        individual = Individual(self.start, self.target)
        path = self.population[i].path[:]
        x, y = smooth(path, points = 50, sigma = random.uniform(0.5, 2.0))
        for i in range(50):
          individual.path.append((float(x[i]), float(y[i])))
        self.population.append(individual)

  def run(self):
    print('\r', "Creating the initial population...", end='')
    self.initialPopulation()
    for i in range(self.GENERATIONS):
      print('\r', "Progress: {0} of {1} Generations {2}".format(i + 1, self.GENERATIONS, (i % 4) * "." + (4 - (i % 4)) * " "), end='')
      self.fitnessFuction()
      self.selection()
      self.crossover()
      self.mutation()
      self.smooth()
      self.avoidCollision()
      self.pathCompression()
    self.fitnessFuction()
    self.selection()
    print('\r', "Status: Complete                    ")
    for individual in self.population[:1]:
      print(" Feasible Solution: ", individual.feasible)
      print(" Path: ", individual.fullPath())
      print(" Score: ", individual.score)
      print(" Distance: ", individual.distance)
      print(" Safety: ", individual.safety)
      print(" Smoothness: ", individual.smoothness)
      print(" Improved Individual: ", individual.new)
