import math, time
from RotatePID import RotatePID

class GoToGoalPID:

  def __init__(self):
    self.kp = 0.2
    self.ki = self.kp / 1000.0
    self.kd = self.kp * 2.0
    self.epsilon = 0.05
    self.rotate_pid = RotatePID()
    self.i_error = []
    self.diff_error = 0
    self.old_error = 0

  def readSensor(self, robot):
    distances = [0 for _ in range(360)]
    reads = [0 for _ in range(360)]
    laser = robot.readLaserSensor()

    # Compute the distance
    for i in range(int(len(laser) / 3)):
        x = laser[3 * i]
        y = laser[3 * i + 1]
        d = (x ** 2 + y ** 2) ** (1.0 / 2.0)
        alpha = (math.atan2(y, x) + (2 * math.pi)) % (2 * math.pi) * (180 / math.pi)
        distances[int(alpha)] += d
        reads[int(alpha)] += 1

    # Average distance per angle
    for i in range(360):
      if reads[i]:
        distances[i] = distances[i] / reads[i]
      else:
        distances[i] = 3.0

    return distances

  def reset(self):
    self.i_error = []
    self.diff_error = 0
    self.old_error = 0

  def go(self, robot, point):
    x, y = point
    distances = [5 for _ in range(360)]
    frontal_distance = min(distances[0:30] + distances[330:360])

    if frontal_distance < 0.5:
      return (0.0, 0.0)
    
    c_x, c_y, _ = robot.getCurrentPosition()
    distance = ((x - c_x) ** 2 + (y - c_y) ** 2) ** (1.0 / 2.0)
    angle = (math.atan2(y - c_y, x - c_x) + (2 * math.pi)) % (2 * math.pi)
    _, _, theta = robot.getCurrentOrientation()
    error = ((angle - theta + math.pi) % (2 * math.pi)) - math.pi

    if distance < self.epsilon:
      return (0.0, 0.0)

    if abs(error) > math.pi / 6.0:
      robot.stop()
      self.rotate_pid.rotate(robot, error)
      return (0.0, 0.0)
    else:
      self.i_error.insert(0, error)
      self.diff_error = error - self.old_error
      self.old_error = error
      u = self.kp * error + self.ki * sum(self.i_error[:20]) + self.kd * self.diff_error
      return (2.0 - u, 2.0 + u)