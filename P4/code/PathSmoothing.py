import numpy as np
from scipy.ndimage import gaussian_filter1d

def smooth(path, points = 100, sigma = 2):
    path = np.array(path)
    x, y = path.T
    # print(x, y)
    t = np.linspace(0, 1, len(x))
    t2 = np.linspace(0, 1, points)
    x2 = np.interp(t2, t, x)
    y2 = np.interp(t2, t, y)
    x3 = gaussian_filter1d(x2, sigma)
    y3 = gaussian_filter1d(y2, sigma)
    x4 = np.interp(t, t2, x3)
    y4 = np.interp(t, t2, y3)
    return x3, y3