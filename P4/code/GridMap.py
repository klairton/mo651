from Evaluator import Evaluator
import random
import sys
sys.setrecursionlimit(3000)

class GridMap():
  def __init__(self, _walls, _obstacles, _GRID_SIZE = 10):
    self.evaluator = Evaluator(_walls, _obstacles)
    self.GRID_SIZE = _GRID_SIZE 
    self.GRID_SQM = 10.0 / self.GRID_SIZE

  def path(self, grid, avaliable, visited, origin, target):
    x , y = origin
    if x < 0 or x > self.GRID_SIZE:
      return []
    if y < 0 or y > self.GRID_SIZE:
      return []
    if avaliable[x][y] and not visited[x][y]:
      visited[x][y] = True
    else:
      return []
    if (x, y) == target:
      return [grid[x][y]]

    map = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1), (x + 1, y + 1), (x + 1, y - 1), (x - 1, y + 1), (x - 1, y - 1)]
    random.shuffle(map)
    
    for p  in map:
      res = self.path(grid, avaliable, visited, p, target)
      if res:
        res.insert(0, grid[x][y])
        return res
    return []

  def gridChoose(self, grid, avaliable, origin, target):
    origin_distance = None
    target_distance = None
    new_origin = None
    new_target = None
    for i in range(self.GRID_SIZE + 1):
      for j in range(self.GRID_SIZE + 1):
        if avaliable[i][j]:
          x1, y1 = grid[i][j]

          x2, y2 = origin
          distance = (((x2 - x1) ** 2) + ((y2 - y1) ** 2)) ** (1.0 / 2.0)
          if origin_distance == None or distance < origin_distance:
            origin_distance = distance
            new_origin = (i, j)

          x2, y2 = target
          distance = (((x2 - x1) ** 2) + ((y2 - y1) ** 2)) ** (1.0 / 2.0)
          if target_distance == None or distance < target_distance:
            target_distance = distance
            new_target = (i, j)
    return (new_origin, new_target)

  def run(self, origin, target, grid):
    self.GRID_SIZE = grid
    self.GRID_SQM = 10.0 / self.GRID_SIZE
    grid = [ [ (i * self.GRID_SQM, j * self.GRID_SQM) for i in range(self.GRID_SIZE + 1)] for j in range(self.GRID_SIZE, -1, -1)]
    avaliable = [ [ self.evaluator.freeSpace((i * self.GRID_SQM, j * self.GRID_SQM)) for i in range(self.GRID_SIZE + 1)] for j in range(self.GRID_SIZE, -1, -1)]
    visited = [ [ False for i in range(self.GRID_SIZE + 1)] for j in range(self.GRID_SIZE, -1, -1)]
    new_origin, new_target = self.gridChoose(grid, avaliable, origin, target)
    return self.path(grid, avaliable, visited, new_origin, new_target)

  def getGridOccupance(self):
    grid = [ [ (i * self.GRID_SQM, j * self.GRID_SQM) for i in range(self.GRID_SIZE + 1)] for j in range(self.GRID_SIZE, -1, -1)]
    occupance = [ [ (self.evaluator.freeSpace((i * self.GRID_SQM, j * self.GRID_SQM))) for i in range(self.GRID_SIZE + 1)] for j in range(self.GRID_SIZE, -1, -1)]
    return (grid, occupance)