import numpy as np
from skimage import io
# import cv2
from skimage.transform import hough_line, hough_line_peaks
from skimage.feature import canny
from skimage import data
from skimage.transform import probabilistic_hough_line
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
from matplotlib import cm
from skimage import img_as_ubyte

def extract_lines(img_name):
    image = io.imread(img_name, flatten=True)
    image = img_as_ubyte(image)
    edges = canny(image, 2, 1, 25)
    lines = probabilistic_hough_line(edges, threshold=10, line_length=5,
                                    line_gap=3)

    # Generating figure 2
    fig, axes = plt.subplots(1, 3, figsize=(15, 5), sharex=True, sharey=True)
    ax = axes.ravel()

    ax[0].imshow(image, cmap=cm.gray)
    ax[0].set_title('Input image')

    ax[1].imshow(edges, cmap=cm.gray)
    ax[1].set_title('Canny edges')

    ax[2].imshow(edges * 0)
    for line in lines:
        p0, p1 = line
        # print(p0,p1)
        ax[2].plot((p0[0], p1[0]), (p0[1], p1[1]))
    ax[2].set_xlim((0, image.shape[1]))
    ax[2].set_ylim((image.shape[0], 0))
    ax[2].set_title('Probabilistic Hough')

    for a in ax:
        a.set_axis_off()

    plt.tight_layout()
    plt.show()


################### CREATING LASER MAP IMAGE ###################
map_laser_ground_truth = np.load("map_laser_ground_truth.npy")
X1 = []
Y1 = []
for x,y in map_laser_ground_truth:
    X1.append(x)
    Y1.append(y)
fig, ax = plt.subplots(figsize=(8, 6))
ax.scatter(X1, Y1, c = 'tab:red', marker = '.')
ax.set_axis_off()
plt.ylim(-8, 8)
plt.xlim(-8, 8)
fig.savefig("map_laser_ground_truth.png")
plt.close(fig)

########################### PLOT ###############################
extract_lines("map_laser_ground_truth.png")
extract_lines("door_1.png")
extract_lines("door_2.png")
