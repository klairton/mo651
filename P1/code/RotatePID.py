import math, time

class RotatePID:


	def __init__(self):
		self.kp = 0.15
		self.ki = self.kp / 50.0
		self.kd = self.kp * 8.0
		self.epsilon = 0.01

	def rotate(self, robot, degrees):

		i_error = 0
		diff_error = 0
		old_error = 0

		a, b, g = robot.read_gyro_sensor()
		origin = (g + 2 * math.pi) % (2 * math.pi)
		target = (origin + degrees + 2 * math.pi) % (2 * math.pi)

		print(origin * (180 / math.pi))
		print(target * (180 / math.pi))

		print("INI: ", ((g + 2 * math.pi) % (2 * math.pi)) * (180 / math.pi))

		while True:
			error = (target - origin + math.pi) % (2 * math.pi) - math.pi
			print("ERROR: ", error)
			if abs(error) < self.epsilon:
				robot.stop()
				break
			i_error += error
			diff_error = error - old_error
			old_error = error
			u = self.kp * error + self.ki * i_error + self.kd * diff_error
			robot.set_left_velocity(-u)
			robot.set_right_velocity(u)
			time.sleep(0.1)
			a, b, g = robot.read_gyro_sensor()
			origin = (g + 2 * math.pi) % (2 * math.pi)
			print(((g + 2 * math.pi) % (2 * math.pi)) * (180 / math.pi))



