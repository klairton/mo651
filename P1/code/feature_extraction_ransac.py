import numpy as np
from matplotlib import pyplot as plt

from sklearn import linear_model, datasets

data = np.load("map_laser_ground_truth.npy")
for i in range(-8, 9, 1):
        for j in range(-8, 9, 1):
                X = []
                Y = []
                for (x,y) in data:
                        if x >= i and x <= i+1 and y >= j and y <= j+1:
                                X += [[x]]
                                Y += [[y]]

                X = np.array(X)
                Y = np.array(Y)
                if len(X) <= 10:
                        continue
                ransac = linear_model.RANSACRegressor(stop_score=0.98, random_state = 0)
                ransac.fit(X, Y)
                inlier_mask = ransac.inlier_mask_
                outlier_mask = np.logical_not(inlier_mask)

                # Predict data of estimated models
                line_X = np.arange(X.min(), X.max(), step=0.01)
                line_X = line_X.reshape(-1,1)
                line_y_ransac = ransac.predict(line_X)
                plt.plot(line_X, line_y_ransac, color='cornflowerblue', linewidth=2)
plt.legend(loc='lower right')
plt.xlabel("x")
plt.ylabel("y")
plt.ylim(-8, 8)
plt.xlim(-8, 8) 
plt.show()
