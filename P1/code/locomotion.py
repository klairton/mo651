from __future__ import print_function

import random
import sys, time, cv2
from matplotlib import pyplot as plt
import numpy as np
import math

sys.path.insert(0, '../src')
from robot import Robot
from utils import *


MAX_ITERATION = 250
iteration = 0

robot = Robot()
x, y, z = robot.get_current_position()
map_laser_odometry = []
map_laser_ground_truth = []
map_sonar_ground_truth = []
map_door_laser_ground_truth = []
odometry = [(x, y)]
ground_truth = [(x, y)]
coord_local = [x, y, z]
alpha, beta, gamma = robot.get_current_orientation()
gamma = (gamma + 2 * math.pi) % (2 * math.pi)


def get_alpha(alpha):
    return (alpha + 2 * math.pi) % (2 * math.pi)

def point_to_global(xLocal, yLocal, x, y, alpha):
   T_trans = [[1, 0, x], [0, 1, y], [0, 0, 1]]
   T_rot = [[math.cos(alpha), -math.sin(alpha), 0], [math.sin(alpha), math.cos(alpha),0], [0,0,1]]
   T = np.matmul(T_trans, T_rot)
   pos = np.matmul(T, np.transpose([xLocal,yLocal,1.0]))
   return pos

def compute_odometry(robot):
    # Odometry
    # Using the gamma angle and wheel circumference we estimate the next stop point
    # a, b, g = robot.get_current_orientation()
    # By our strategy, our robot or rotate or is moving foward
    d = (2 * math.pi * robot.WHEEL_RADIUS) / 360 * ((180 / math.pi * 2))
    coord_local[0] = d * math.cos(gamma) + odometry[-1][0]
    coord_local[1] = d * math.sin(gamma) + odometry[-1][1]
    odometry.append(coord_local[:2])

def compute_ground_truth(robot):
    # Ground Truth
    x, y, z = robot.get_current_position()
    ground_truth.append((x, y))

def obstacle_detect_sonar(robot, sonars = [0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0], distance = 0.5):
    ultrassonic = robot.read_ultrassonic_sensors()
    detect = []
    for i in range(16):
        if sonars[i]:
            detect.append(ultrassonic[i] < distance)
        else:
            detect.append(False)
    return any(detect)

def obstacle_detect_laser(robot, distance = 0.8):
    laser = robot.read_laser()
    data = []
    out = []
    for i in range(int(len(laser) / 3)):
        data.append((laser[3 * i], laser[3 * i + 1]))
    for x, y in data:
        if x >= 0:
            if y >= 0:
                d = (x**2 + y**2)**(1.0/2)
                alpha = math.asin(y / d) * 180/math.pi
                out.append((x, y, d, alpha))
            else:
                d = (x**2 + y**2)**(1.0/2)
                alpha = (math.asin(x / d) + math.pi + math.pi / 2) * 180/math.pi
                out.append((x, y, d, alpha))
        else:
            if y >= 0:
                d = (x**2 + y**2)**(1.0/2)
                alpha = (math.asin(abs(x) / d) + math.pi / 2) * 180/math.pi
                out.append((x, y, d, alpha))
            else:
                d = (x**2 + y**2)**(1.0/2)
                alpha = (math.asin(abs(y) / d) + math.pi) * 180/math.pi
                out.append((x, y, d, alpha))
    for x, y, d, alpha in out:
        if (alpha < 30 or alpha > 330) and d < distance:
            return True
    return False

def door_scan(robot):
    laser = robot.read_laser()
    data = []
    out = []
    for i in range(int(len(laser) / 3)):
        data.append((laser[3 * i], laser[3 * i + 1]))
    for x, y in data:
        if x >= 0:
            if y >= 0:
                d = (x**2 + y**2)**(1.0/2)
                alpha = math.asin(y / d) * 180/math.pi
                out.append((x, y, d, alpha))
            else:
                d = (x**2 + y**2)**(1.0/2)
                alpha = (math.asin(x / d) + math.pi + math.pi / 2) * 180/math.pi
                out.append((x, y, d, alpha))
        else:
            if y >= 0:
                d = (x**2 + y**2)**(1.0/2)
                alpha = (math.asin(abs(x) / d) + math.pi / 2) * 180/math.pi
                out.append((x, y, d, alpha))
            else:
                d = (x**2 + y**2)**(1.0/2)
                alpha = (math.asin(abs(y) / d) + math.pi) * 180/math.pi
                out.append((x, y, d, alpha))
    v = []
    for x_local, y_local, d, alpha in out:
        if (alpha < 30 or alpha > 330) and d < 2:
            x, y, z = robot.get_current_position()
            a, b, g = robot.get_current_orientation()
            p = point_to_global(x_local, y_local, x, y, get_alpha(g))
            v.append((p[0], p[1]))
    if len(v):
        map_door_laser_ground_truth.append(compute_centroid(v))

def laser_capture(robot):
    data = []
    x, y, z = robot.get_current_position()
    a, b, g = robot.get_current_orientation()
    laser = robot.read_laser()
    for i in range(int(len(laser) / 3)):
        p = point_to_global(laser[3 * i], laser[3 * i + 1], x, y, get_alpha(g))
        map_laser_ground_truth.append((p[0], p[1]))
        p = point_to_global(laser[3 * i], laser[3 * i + 1], coord_local[0], coord_local[1], gamma)
        map_laser_odometry.append((p[0], p[1]))

def sonar_capture(robot):
    # angles of each sonar
    sonar_angles = [90,50,30,10,-10,-30,-50,-90,-90,-130,-150,-170,170,150,130,90]
    NO_DETECTION_DIST = 5.0
    data = []
    x, y, z = robot.get_current_position()
    a, b, g = robot.get_current_orientation()
    dist = robot.read_ultrassonic_sensors()
    for i in range(0,16):
        if dist[i] < NO_DETECTION_DIST:
            gamma = get_alpha(get_alpha(g) + get_alpha(math.radians(sonar_angles[i])))
            # using formula of slide Locomotion II page 39
            # 0.0975 is the robot radius
            px = (x +((dist[i]+0.0975)*math.cos(gamma)))
            py = (y +((dist[i]+0.0975)*math.sin(gamma)))
            map_sonar_ground_truth.append((px, py))

def compute_centroid(points):
    c_x = 0.0
    c_y = 0.0
    for x, y in points:
        c_x += x
        c_y += y
    return (c_x / len(points), c_y / len(points))

def door_detect(robot):
    resolution, raw_img = robot.read_vision_sensor()
    image = vrep2array(raw_img, resolution)
    lower = np.array((0, 0, 180), dtype = "uint8")
    upper = np.array((35, 35, 255), dtype = "uint8")
    mask = cv2.inRange(image, lower, upper)
    points = []
    for i in range(len(mask) - 1, -1, -1):
        for j in range(0, len(mask[i])):
            if mask[i][j] != 0:
                points.append((j, i))
    return (len(points) > 400, points)

# Initial map
laser_capture(robot)
sonar_capture(robot)

while iteration < MAX_ITERATION:

    # First, try to identify a possible door with clear path using the frontal camera and sonars
    door, points = door_detect(robot)
    while door and not obstacle_detect_laser(robot, distance=1.0) and iteration < MAX_ITERATION:
        c_x, c_y = compute_centroid(points)
        if c_x < 118:
            robot.rotate((-5 * (2 * math.pi) / 360)) # Rotate ~5 degrees
            gamma = (gamma + (5 * (math.pi / 180)) + (2 * math.pi)) % (2 * math.pi)
            print("Camera - Rotating...")
            print("Odometry Gamma:", gamma * (180 / math.pi))
            print("Ground Truth Gamma:", get_alpha(robot.get_current_orientation()[2]) * (180 / math.pi))
        elif c_x > 138:
            robot.rotate((5 * (2 * math.pi) / 360)) # Rotate ~-5 degrees
            gamma = (gamma + (-5 * (math.pi / 180)) + (2 * math.pi)) % (2 * math.pi)
            print("Camera - Rotating...")
            print("Odometry Gamma:", gamma * (180 / math.pi))
            print("Ground Truth Gamma:", get_alpha(robot.get_current_orientation()[2]) * (180 / math.pi))
        elif c_x >= 118 and c_x <= 138:
            print("Iteration:", iteration, "of", MAX_ITERATION)

            # Odometry
            compute_odometry(robot)

            # The robot goes ahead for 2 seconds
            robot.set_left_velocity(1.0)
            robot.set_right_velocity(1.0)
            time.sleep(2)
            laser_capture(robot)
            sonar_capture(robot)

            # Ground Truth
            compute_ground_truth(robot)

            print("Odometry:", odometry[-1])
            print("Ground Truth:", ground_truth[-1])
            print("Odometry Gamma:", gamma * (180 / math.pi))
            print("Ground Truth Gamma:", get_alpha(robot.get_current_orientation()[2]) * (180 / math.pi))
            door_scan(robot)
            iteration += 1
        door, points = door_detect(robot)

    # Otherwise, rotate until finding a free path

    # Considering the lateral sonars, the robot will rotate clockwise or not
    sonars = [-1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,-1]
    ultrassonic = robot.read_ultrassonic_sensors()
    clockwise = 0
    for i in range(16):
        clockwise += sonars[i] * ultrassonic[i]
    if clockwise == 0:
        clockwise = random.choice([-1.0, 1.0]) # if have space on both sides, choose randomly
    else:
        clockwise = clockwise / abs(clockwise) # result: -1.0 or 1.0

    while obstacle_detect_laser(robot): # Object identified just ahead
        # Rotate the robot to find a clear path
        robot.stop()
        robot.rotate(clockwise * (15 * (math.pi / 180))) # Rotate ~15 degrees * orientation
        gamma = (gamma + (-clockwise * 15 * (math.pi / 180)) + (2 * math.pi)) % (2 * math.pi)
        print("Sonar - Rotating...")
        print("Odometry Gamma:", gamma * (180 / math.pi))
        print("Ground Truth Gamma:", get_alpha(robot.get_current_orientation()[2]) * (180 / math.pi))
        laser_capture(robot)
        sonar_capture(robot)

    print("Iteration:", iteration, "of", MAX_ITERATION)

    # Odometry
    compute_odometry(robot)

    # The robot goes ahead for 2 seconds
    robot.set_left_velocity(1.0)
    robot.set_right_velocity(1.0)
    time.sleep(2)
    laser_capture(robot)
    sonar_capture(robot)

    # Ground Truth
    compute_ground_truth(robot)

    print("Odometry:", odometry[-1])
    print("Ground Truth:", ground_truth[-1])
    print("Odometry Gamma:", gamma * (180 / math.pi))
    print("Ground Truth Gamma:", get_alpha(robot.get_current_orientation()[2]) * (180 / math.pi))
    iteration += 1

print("Simulation Finished.")
robot.stop()

# graph plot and save points
np.save("map_door_laser_ground_truth.npy", map_door_laser_ground_truth)
np.save("map_laser_ground_truth.npy", map_laser_ground_truth)
np.save("map_laser_odometry.npy", map_laser_odometry)
np.save("map_sonar_ground_truth.npy", map_sonar_ground_truth)
np.save("odometry.npy", odometry)
np.save("ground_truth.npy", ground_truth)
