# coding: utf-8
from matplotlib import pyplot as plt
import numpy as np

###################PLOT 1###################

odometry = np.load("odometry.npy")
ground_truth = np.load("ground_truth.npy")

X1 = []
Y1 = []
for x,y in odometry:
    X1.append(x)
    Y1.append(y)

X2 = []
Y2 = []
for x,y in ground_truth:
    X2.append(x)
    Y2.append(y)

fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(X1, Y1, c = 'tab:red', marker = '.', label = "Odometria")
ax.scatter(X2, Y2, c = 'tab:green', marker = '.', label = "Ground Truth")
ax.grid(True)
ax.legend()
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.ylim(-8, 8)
plt.xlim(-8, 8) 
plt.title("Odometria vs. Ground Truth")
plt.show()

###################PLOT 2###################

map_door_laser_ground_truth = np.load("map_door_laser_ground_truth.npy")
map_laser_ground_truth = np.load("map_laser_ground_truth.npy")

X1 = []
Y1 = []
for x,y in map_laser_ground_truth:
    X1.append(x)
    Y1.append(y)

X2 = []
Y2 = []
for x,y in map_door_laser_ground_truth:
    X2.append(x)
    Y2.append(y)

fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(X1, Y1, c = 'tab:red', marker = '.')
ax.scatter(X2, Y2, c = 'tab:blue', marker = 'x', label = "Door Region")
ax.grid(True)
ax.legend()
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.ylim(-8, 8)
plt.xlim(-8, 8) 
plt.title("Laser point cloud with door detection (Ground Truth)")
plt.show()

###################PLOT 3###################

map_laser_odometry = np.load("map_laser_odometry.npy")

X1 = []
Y1 = []
for x,y in map_laser_odometry:
    X1.append(x)
    Y1.append(y)

fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(X1, Y1, c = 'tab:red', marker = '.')
ax.grid(True)
ax.legend()
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.ylim(-8, 8)
plt.xlim(-8, 8) 
plt.title("Laser point cloud (Odometry)")
plt.show()

###################PLOT 4###################

map_sonar_ground_truth = np.load("map_sonar_ground_truth.npy")

X1 = []
Y1 = []
for x,y in map_sonar_ground_truth:
    X1.append(x)
    Y1.append(y)

fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(X1, Y1, c = 'tab:red', marker = '.')
ax.grid(True)
ax.legend()
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.ylim(-8, 8)
plt.xlim(-8, 8) 
plt.title("Sonar point cloud (Ground Truth)")
plt.show()